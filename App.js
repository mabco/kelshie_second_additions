// Main imports
import * as React from 'react';
import { Platform, StatusBar, StyleSheet, View } from 'react-native';
import useCachedResources from './hooks/useCachedResources';

// Main store functionality imports
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import ReduxThunk from 'redux-thunk';
import CategoriesReducer from './store/Reducers/Categories';
import ProductsReducer from './store/Reducers/Products';
import CartReducer from './store/Reducers/Cart';
import OrdersReducer from './store/Reducers/Orders';
import ShopsReducer from './store/Reducers/Shops';
import UserReducer from './store/Reducers/User';
import BrandsReducer from './store/Reducers/Brands';

// Main navigator import
import ShopNavigator from './navigation/ShopNavigator';

const RootReducer = combineReducers({
    categoriesReducer: CategoriesReducer,
    productsReducer: ProductsReducer,
    cartReducer: CartReducer,
    ordersReducer: OrdersReducer,
    shopsReducer: ShopsReducer,
    userReducer: UserReducer,
    brandsReducer: BrandsReducer,
});

const Store = createStore(RootReducer, applyMiddleware(ReduxThunk));

export default function App() {
    const IsLoadingComplete = useCachedResources();

    if (!IsLoadingComplete) {
        return null;
    } else {
        return (
            <Provider store={Store}>
                <View style={styles.container}>
                    {Platform.OS === 'ios' && <StatusBar barStyle="dark-content" />}
                    <ShopNavigator/>
                </View>
            </Provider>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
});
