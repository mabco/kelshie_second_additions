import * as React from 'react';
import { Text } from 'react-native';

export function MonoText(props) {
    return <Text {...props} style={[props.style, props.isArabic ? { fontFamily: 'space-mono' } : { fontFamily: 'space-mono' }]} />;
}
export function CustomText(props) {
    return <Text {...props} style={[props.style, props.isArabic ? { fontFamily: 'AFont' } : { fontFamily: 'EFont' }]} />;
}
