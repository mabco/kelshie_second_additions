import React, { useState } from 'react';
import { View, Image, StyleSheet, TouchableNativeFeedback, TouchableOpacity, Platform, Dimensions } from 'react-native';
import Colors from '../../constants/Colors';
import { CustomText } from '../UI/StyledText';

const ShopItem = props => {
    let TouchableItem = TouchableOpacity;

    if (Platform.OS === 'android' && Platform.Version >= 21)
        TouchableItem = TouchableNativeFeedback;

    const MaxStartCount = 5;
    const FilledStarsCount = props.rating;
    const EmptyStarsCount = MaxStartCount - FilledStarsCount;
    const Stars = [];
    //const { width: screenWidth } = Dimensions.get('window');

    const { width, height } = Dimensions.get('window');
    console.log("w",width)
    console.log("h",height)
    for (let index = 0; index < FilledStarsCount; index++) {
        Stars.push(<Image key={index} style={styles.star} source={require('../../assets/images/icons/filled-star.png')} />);
    }

    for (let index = FilledStarsCount; index < EmptyStarsCount + FilledStarsCount; index++) {
        Stars.push(<Image key={index} style={styles.star} source={require('../../assets/images/icons/empty-star.png')} />);
    }

    const [valid, setValid] = useState(true);

    return (
        <View style={styles.card} >
            {/* <Image source={require('../../assets/images/AYA.png')} style={styles.absoluteFillObject} resizeMode='cover' /> */}

            <TouchableItem onPress={props.onViewShop} useForeground>
                <View>
                    <Image onError={() => setValid(false)} style={styles.cardImage} source={valid ? { uri: props.image } : require('../../assets/images/shop-banner-placeholder.jpg')} />
                    <View style={styles.cardHeader}>
                        <View>
                        {/* <View style={styles.inlineItem}> */}
                            <Image style={styles.logoImage} source={{ uri: props.logo }} />
                            <CustomText isArabic={true} style={styles.title}>{props.name}</CustomText>
                        </View>
                    </View>

                    <View style={styles.lineContainer}>
                        {/* <View style={styles.Line}></View> */}
                    </View>

                    <View style={styles.cardFooter}>
                        <View style={styles.socialBarContainer}>
                            <View style={styles.socialBarSection}>
                                <View style={styles.starContainer}>
                                    {Stars}
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
            </TouchableItem>
        </View>
    );
};

const styles = StyleSheet.create({
    /******** card **************/
    card: {
        shadowRadius: 10,
        elevation: 3,
        marginVertical: 8,
        // backgroundColor: "trancp",
        borderRadius: 10,
        borderTopColor: '#000000',
        borderLeftColor: '#000000',
        borderRightColor: '#000000',
        borderBottomColor: '#000000',
        borderWidth: 2,
        overflow: 'hidden'
    },
    cardHeader: {
        paddingVertical: 8,
        paddingHorizontal: 16,
        borderTopLeftRadius: 1,
        borderTopRightRadius: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    cardFooter: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 2,
        paddingBottom: 10,
        paddingHorizontal: 0,
        borderBottomLeftRadius: 1,
        borderBottomRightRadius: 1,
        backgroundColor:Colors.activeSelected,
       

    },
    cardImage: {
        flex: 1,
        height: 150,
        width: null,
        borderTopRightRadius: 5,
        borderTopLeftRadius: 5,
    },
    /******** card components **************/
    title: {
        fontSize: 25,
        flex: 1,

    },
    description: {
        fontSize: 20,
        fontWeight: 'bold',
        color: "#888",
        flex: 1,
        marginBottom: 5,
    },
    /******** social bar ******************/
    socialBarContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        flex: 1
    },
    socialBarSection: {
        justifyContent: 'center',
        flexDirection: 'row',
        marginHorizontal: 10,
        // marginTop: -10
    },
    logoImage: {
        width: 75,
        height: 75,
        borderRadius: 40,
        marginRight: 10,
        borderColor: '#eee',
        borderWidth: 2
    },
    inlineItem: {
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 2,
        marginLeft: -5
    },
    starContainer: {
        justifyContent: 'center',
        flexDirection: 'row',
        backgroundColor:Colors.activeSelected,
    },
    star: {
        width: 30,
        height: 30,
    },
    lineContainer: {
        flex: 1,
        alignItems: 'center',
        paddingBottom: 15,
        backgroundColor:Colors.activeSelected,
    },
    Line: {
        height: 2,
        width: '60%',
        backgroundColor: '#efefef'
    },
    absoluteFillObject: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 5
    }
});

export default ShopItem;