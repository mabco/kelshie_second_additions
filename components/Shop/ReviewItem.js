import React, { useState } from 'react';
import { View,StyleSheet, Image } from 'react-native';
import Colors from '../../constants/Colors';
import { TouchableOpacity } from 'react-native-gesture-handler';
import {CustomText } from '../../components/UI/StyledText';

const ReviewItem = props => {
    const MaxStartCount = 5;
    const FilledStarsCount = props.rating;
    const EmptyStarsCount = MaxStartCount - FilledStarsCount;
    const Stars = [];

    for (let index = 0; index < FilledStarsCount; index++) {
        Stars.push(<Image key={index} style={styles.star} source={require('../../assets/images/icons/filled-star.png')} />);
    }

    for (let index = FilledStarsCount; index < EmptyStarsCount + FilledStarsCount; index++) {
        Stars.push(<Image key={index} style={styles.star} source={require('../../assets/images/icons/empty-star.png')} />);
    }

    const [IsViewMore, SetIsViewMore] = useState(false);

    return (
        <View style={styles.commentcontainer}>
            <Image style={styles.image} source={require('../../assets/images/user-profile-image.png')} />

            <View style={styles.content}>
                <View style={styles.contentHeader}>
                     <CustomText isArabic={true} style={styles.name}>{props.name}</CustomText>
                     <CustomText isArabic={true} style={styles.time}>{props.time}</CustomText>
                </View>
                <View>
                     <CustomText isArabic={true}>{props.comment.substring(0, 100)}</CustomText>
                    {IsViewMore &&  <CustomText isArabic={true}>{props.comment.substring(100)}</CustomText>}
                    {props.comment.length > 100 &&
                        <TouchableOpacity onPress={() => SetIsViewMore(prevState => !prevState)}>
                            <CustomText isArabic={true} style={{ color: '#0f04cc' }}>Read {IsViewMore && 'Less'} {!IsViewMore && 'More'}</CustomText>
                        </TouchableOpacity>
                    }
                </View>
                <View style={styles.commentstarts}>
                    {Stars}
                </View>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    commentstarts: {
        flexDirection: 'row',
        marginBottom: 5,
    },
    commentcontainer: {
        paddingVertical: 10,
        paddingHorizontal: 10,
        marginHorizontal: 5,
        marginBottom: 10,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'flex-start',
        backgroundColor: "#fff",
    },
    content: {
        marginLeft: 16,
        flex: 1,
    },
    contentHeader: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 6,
    },
    image: {
        width: 43,
        height: 45,
        borderRadius: 50,
    },
    time: {
        marginRight: 8,
        fontSize: 11,
        color: "#808080",
    },
    name: {
        fontSize: 18,
        flexWrap: 'wrap',
        flex: 1
    },
    star: {
        marginTop: 10,
        width: 20,
        height: 20,
    },
});

export default ReviewItem;