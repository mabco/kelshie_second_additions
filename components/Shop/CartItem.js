import React from 'react';
import { View,StyleSheet, TouchableOpacity, Platform } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import {CustomText } from '../../components/UI/StyledText';

const CartItem = props => {
    console.log(props.title)
    return (
        <View style={styles.cartItem}>
            <View style={styles.itemData}>
            {/* <CustomText isArabic={true} style={styles.quantity}>{props.quantity} </CustomText> */}
            <CustomText isArabic={true} style={styles.mainText}>{props.title}</CustomText>
            </View>
            <View style={styles.itemData}>
            <CustomText isArabic={true} style={styles.mainText}>{props.amount} SP</CustomText>
                {props.removable && 
                <TouchableOpacity onPress={props.onRemove} style={styles.deleteButton}>
                    <Ionicons
                        name={Platform.OS === 'android' ? 'md-trash' : 'ios-trash'}
                        size={23}
                        color="red"
                    />
                </TouchableOpacity>}
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    cartItem: {
        padding: 10,
        backgroundColor: 'white',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginHorizontal: 20
    },
    itemData: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    quantity: {
        color: '#888',
        fontSize: 16
    },
    mainText: {
        fontWeight: 'bold',
        fontSize: 16
    },
    deleteButton: {
        marginLeft: 20
    }
});

export default CartItem;
