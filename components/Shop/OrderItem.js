import React, { useState } from 'react';
import { View,Button, StyleSheet } from 'react-native';
import CartItem from './CartItem';
import Colors from '../../constants/Colors';
import {CustomText } from '../../components/UI/StyledText';

const OrderItem = props => {
    const [showDetails, setShowDetails] = useState(false);

    return (
        <View style={styles.orderItem}>
            <View style={styles.summary}>
            <CustomText isArabic={true}  style={styles.date}>{props.date}</CustomText>
                <Button
                    color={Colors.tintColor}
                    title={showDetails ? 'Hide Details' : 'Show Details'}
                    onPress={() => setShowDetails(prevState => !prevState)}
                />
            </View>
            {showDetails && (
                <View style={styles.detailItems}>
                    {props.items.map(cartItem => (
                        <CartItem
                            key={cartItem.productID}
                            quantity={cartItem.quantity}
                            amount={cartItem.sum}
                            title={cartItem.productName}
                        />
                    ))}
                </View>
            )}
            <View style={styles.footer}>
            <CustomText isArabic={false} style={styles.total}>Total</CustomText>
            <CustomText isArabic={false} style={styles.totalAmount}>{props.amount.toFixed(2)} SP</CustomText>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    orderItem: {
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 8,
        elevation: 5,
        borderRadius: 10,
        backgroundColor: 'white',
        margin: 20,
        padding: 10,
        marginTop: 5,
        alignItems: 'center'
    },
    summary: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%',
        marginBottom: 15
    },
    totalAmount: {
        fontSize: 16,
        fontWeight: 'bold',
    },
    date: {
        fontSize: 16,
        color: '#888',
        marginTop: -17
    },
    detailItems: {
        width: '100%'
    },
    footer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%',
        backgroundColor: '#DDD',
        paddingHorizontal: 20,
        paddingVertical: 10,
        borderRadius: 10,
    },
    total: {
        fontSize: 20,
        color: '#444'
    },
});

export default OrderItem;