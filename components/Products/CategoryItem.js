import React from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';
import { CustomText } from '../../components/UI/StyledText';

const CategoryItem = props => {
    return (
        <TouchableOpacity onPress={props.onViewCategory}>
            <View style={styles.card}>
                <Image style={styles.cardImage} source={{ uri: props.image }} />
                
                <View style={styles.cardContent}>
                    <View>
                    <CustomText isArabic={true} style={styles.name}>{props.name}</CustomText>
                    </View>
                </View>
            </View>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    card: {
        margin: 0,
        borderRadius: 2,
        borderWidth: 1,
        borderColor: "#DCDCDC",
        backgroundColor: "#DCDCDC",
    },
    cardHeader: {
        paddingVertical: 17,
        paddingHorizontal: 16,
        borderTopLeftRadius: 1,
        borderTopRightRadius: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    cardContent: {
        paddingVertical: 12.5,
        paddingHorizontal: 16,
        flex: 1,
        display: 'flex',
        alignContent: 'center',
        height: 200,
        width: null,
        position: 'absolute',
        zIndex: 100,
        left: 0,
        right: 0,
        backgroundColor: 'transparent'
    },
    cardFooter: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 15,
        paddingBottom: 0,
        paddingVertical: 7.5,
        paddingHorizontal: 0
    },
    cardImage: {
        flex: 1,
        height: 150,
        width: null,
    },
    /******** card components **************/
    name: {
        fontSize: 22,
        color: "#ffffff",
        marginTop: 10,
        fontWeight: 'bold'
    },
    time: {
        fontSize: 13,
        color: "#ffffff",
        marginTop: 5
    },
    icon: {
        width: 25,
        height: 25,
    },
    /******** social bar ******************/
    socialBarContainer: {
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        flexDirection: 'row',
        flex: 1
    },
    socialBarSection: {
        justifyContent: 'flex-start',
        flexDirection: 'row',
        flex: 1,
    },
    socialBarlabel: {
        marginLeft: 8,
        alignSelf: 'flex-start',
        justifyContent: 'center',
        color: "#ffffff",
    },
    socialBarButton: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    }
});

export default CategoryItem;