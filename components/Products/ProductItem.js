import React, { useState, useEffect, useCallback } from 'react';
import { View, Text, Image, StyleSheet, TouchableNativeFeedback, TouchableOpacity, Platform } from 'react-native';
import Colors from '../../constants/Colors';
import { CustomText } from '../../components/UI/StyledText';
import ShopNavigator from '../../navigation/ShopNavigator'
import { useSelector, useDispatch } from 'react-redux';

const ProductItem = props => {
    let TouchableItem = TouchableOpacity;
    let Shops = props.shop_products
    // console.log("Shopsjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj",Shops)
    if (Platform.OS === 'android' && Platform.Version >= 21)
        TouchableItem = TouchableNativeFeedback;
    function OnLogoClick() {
        if (Shops.length == 0) {
            alert('Open Shop Page')
        }
        else navigation.navigate('ShopsScreenProduct', { ShopsScreenProduct: Shops })

    }



    return (
        <View style={styles.card} >
            <TouchableItem onPress={props.onPress} useForeground>
                <View>
                    <View style={styles.cardHeader}>
                        <View>
                            <CustomText isArabic={true} style={styles.title}>{props.name}</CustomText>

                            {/* <TouchableOpacity onPress={props.onViewBrandPress}>
                                <CustomText isArabic={true} style={styles.brand}>{props.brand}
                                </CustomText>
                            </TouchableOpacity> */}



                            <TouchableOpacity
                                underlayColor="#cccccc"
                                onPress={props.onViewBrandPress}
                                style={styles.brandItem}>
                                <View style={styles.icon_container} >
                                    <Image style={styles.photo} source={{ uri: props.brandIcon }} />
                                </View>
                                {/* <CustomText isArabic={false} style={styles.title}>{props.brand}</CustomText> */}
                            </TouchableOpacity>
                        </View>

                        {props.showShopLogo &&
                            <TouchableOpacity onPress={() => OnLogoClick()}>
                                <Image style={styles.shopLogo} source={{ uri: props.shopLogo }} />
                            </TouchableOpacity>
                        }
                    </View>

                    <View style={styles.cardImageContainer}>
                        <Image style={styles.cardImage} source={{ uri: props.image }} />
                    </View>

                    <View style={styles.cardFooter}>
                        <View style={styles.socialBarContainer}>
                            <View style={styles.socialBarSection}>
                                <CustomText isArabic={true} style={styles.price}>{props.price}</CustomText>
                            </View>

                            <View style={styles.socialBarSection}>
                                <TouchableOpacity style={styles.socialBarButton2} onPress={props.onAddToCartPress}>
                                    <Image style={styles.icon} source={require('../../assets/images/icons/add-to-cart.png')} />
                                </TouchableOpacity>
                            </View>



                        </View>
                    </View>
                </View>
            </TouchableItem>
        </View>
    );
};

const styles = StyleSheet.create({
    /******** card **************/
    card: {
        shadowColor: '#00000021',
        shadowOffset: {
            width: 2
        },
        shadowOpacity: 0.5,
        shadowRadius: 4,
        marginVertical: 8,
        backgroundColor: "white",
        flexBasis: '45%',
        marginHorizontal: 5,
        borderRadius: 10,
        overflow: 'hidden',
        width: 200,
    },
    cardHeader: {
        paddingVertical: 17,
        paddingHorizontal: 10,
        paddingTop: 10,
        borderTopLeftRadius: 1,
        borderTopRightRadius: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    shopLogo: {
        width: 30,
        height: 33,
        borderRadius: 30,
        borderColor: '#efefef',
        borderWidth: 1,
    },
    cardContent: {
        paddingVertical: 12.5,
        paddingHorizontal: 16,
    },
    cardFooter: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 12.5,
        paddingBottom: 12,
        paddingHorizontal: 16,
        borderBottomLeftRadius: 1,
        borderBottomRightRadius: 1,
    },
    split: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 12.5,
        paddingBottom: 12,
        paddingHorizontal: 16,
        borderBottomLeftRadius: 1,
        borderBottomRightRadius: 1,
    },
    cardImageContainer: {
        flex: 1,
        alignSelf: 'center'
    },
    cardImage: {
        height: 150,
        width: 150,
    },
    /******** card components **************/
    title: {
        fontSize: 17,
        flex: 1,

        color: Colors.Producttitle,
    },
    brand: {
        fontSize: 15,
        flex: 1,
        color: "#000000",
        marginTop: -5,
    },
    price: {
        fontSize: 14,
        fontWeight: 'bold',
        color: "#000000",
        marginTop: 5,
        marginBottom: -15,
    },
    buyNow: {
        color: "purple",
    },
    icon: {
        width: 25,
        height: 25,
        marginLeft: 60
    },
    /******** social bar ******************/
    socialBarContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        flex: 1
    },
    socialBarSection: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        flex: 1,
    },
    socialBarlabel: {
        marginLeft: 8,
        alignSelf: 'flex-end',
        justifyContent: 'center',
    },
    socialBarButton: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    socialBarButton2: {
        padding: 15,
        paddingBottom: 0,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'flex-start',
    },

    brandItem: {
        marginBottom: 0
    },
    icon_container: {
        backgroundColor: '#ccc',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 20,
        borderWidth: 1,
        borderColor: Colors.activeSelected,
        borderRadius: 60,
        margin: 5,
        height: 30,
        width: 30,
        backgroundColor: '#fff',
    },
    photo: {
        width: 40,
        height: 40,
    },
    title: {
        flex: 1,
        fontSize: 10,
        fontWeight: 'bold',
        textAlign: 'center',
        color: '#444444',
        marginRight: 5,
        marginLeft: 5,
    },
});

export default ProductItem;