import React from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';
import Colors from '../../constants/Colors';
import { CustomText } from '../UI/StyledText';

const BrandItem = props => {
    // let navigation=props.navigation
    // let BrandID=props.BrandID
    // console.log(BrandID)
    return (
        <TouchableOpacity
            underlayColor="#cccccc"
            onPress={props.onBrandPress}
            style={styles.brandItem}>
            <View style={styles.icon_container} >
                <Image style={styles.photo} source={{ uri: props.image }} />
            </View>
            <CustomText isArabic={false} style={styles.title}>{props.name}</CustomText>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    brandItem: {
        marginBottom: 10
    },
    icon_container: {
        backgroundColor: '#ccc',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 20,
        borderWidth: 2,
        borderColor: Colors.activeSelected,
        borderRadius: 60,
        margin: 10,
        height: 120,
        width: 120,
        backgroundColor: '#fff',
    },
    photo: {
        width: 100,
        height: 100,
    },
    title: {
        flex: 1,
        fontSize: 17,
        fontWeight: 'bold',
        textAlign: 'center',
        color: '#444444',
        marginTop: 3,
        marginRight: 5,
        marginLeft: 5,
    },
});

export default BrandItem;