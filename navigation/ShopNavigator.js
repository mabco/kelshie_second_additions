// Main import
import * as React from 'react';

// Navigation imports
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';

// Navigators imports
import DrawerNavigator from './DrawerNavigator';
import BottomTabNavigator from './BottomTabNavigator';
import LinkingConfiguration from './LinkingConfiguration';

// Screens imports
import ProductsScreen from '../screens/Products/ProductsScreen';
import ProductOverviewScreen from '../screens/Products/ProductOverviewScreen';
import CartScreen from '../screens/Shops/CartScreen';
import ShopOverviewScreen from '../screens/Shops/ShopOverviewScreen';
import VerificationScreen from '../screens/User/VerificationScreen';
import ShopsScreenProduct from '../screens/Shops/ShopsScreenProduct'
import CategoriesScreenFromBrand from '../screens/Products/CategoriesScreenFromBrand'
import ProductsScreenBrand from '../screens/Products/ProductsScreenBrand'
const Stack = createStackNavigator();
const INITIAL_STACK_ROUTE_NAME = 'DrawerNavigator';

export default function ShopNavigator(route) {
    return (
        <NavigationContainer linking={LinkingConfiguration}>
            <Stack.Navigator initialRouteName={INITIAL_STACK_ROUTE_NAME}>
                <Stack.Screen name="DrawerNavigator" component={DrawerNavigator} />
                <Stack.Screen name="BottomTabNavigator" component={BottomTabNavigator} />
                <Stack.Screen name="ProductsScreen" component={ProductsScreen} />
                <Stack.Screen name="ProductOverviewScreen" component={ProductOverviewScreen} />
                <Stack.Screen name="CartScreen" component={CartScreen} />
                <Stack.Screen name="ShopOverviewScreen" component={ShopOverviewScreen} />
                <Stack.Screen name="VerificationScreen" component={VerificationScreen} />
                <Stack.Screen name="ShopsScreenProduct" component={ShopsScreenProduct} />
                <Stack.Screen name="CategoriesScreenFromBrand" component={CategoriesScreenFromBrand} options={({ route }) => ({ title: route.params.name })} />
                <Stack.Screen name="ProductsScreenBrand" component={ProductsScreenBrand}  />
                
            </Stack.Navigator>
        </NavigationContainer>
    );
}