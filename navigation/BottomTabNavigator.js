// Main imports
import * as React from 'react';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import TabBarIcon from '../components/UI/TabBarIcon';

// Screens imports
import CategoriesScreen from '../screens/Products/CategoriesScreen';
import ShopsScreen from '../screens/Shops/ShopsScreen';
import HomeScreen from '../screens/HomeScreen';
import SwappedDevicesScreen from '../screens/User/SwappedDevicesScreen';
import OrdersScreen from '../screens/User/OrdersScreen';

const BottomTab = createMaterialBottomTabNavigator();
const INITIAL_ROUTE_NAME = 'HomeScreen';

export default function BottomTabNavigator({ navigation, route }) {
    navigation.setOptions({ headerTitle: getHeaderTitle(route) });

    return (
        <BottomTab.Navigator initialRouteName={INITIAL_ROUTE_NAME}
            activeColor="#FFEEDF"
            inactiveColor="#000"
            barStyle={{ backgroundColor: '#000' }}
            sceneAnimationEnabled={true}>
            <BottomTab.Screen
                name="CategoriesScreen"
                component={CategoriesScreen}
                options={{
                    title: 'Categories',
                    tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name="ios-list" />,
                }}
            />

            <BottomTab.Screen
                name="ShopsScreen"
                component={ShopsScreen}
                options={{
                    title: 'Shops',
                    tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name="ios-business" />,
                }}
            />

            <BottomTab.Screen
                name="HomeScreen"
                component={HomeScreen}
                options={{
                    title: 'Home',
                    tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name="ios-home" />,
                }}
            />

            <BottomTab.Screen
                name="SwappedDevicesScreen"
                component={SwappedDevicesScreen}
                options={{
                    title: 'Mobile Swap',
                    tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name="ios-swap" />,
                }}
            />

            <BottomTab.Screen
                name="OrdersScreen"
                component={OrdersScreen}
                options={{
                    title: 'My Orders',
                    tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name="ios-paper" />,
                }}
            />
        </BottomTab.Navigator>
    );
}

function getHeaderTitle(route) {
    const routeName = route.state?.routes[route.state.index]?.name ?? INITIAL_ROUTE_NAME;

    switch (routeName) {
        case 'CategoriesScreen':
            return 'Categories Page';
        case 'ShopsScreen':
            return 'Shops Page';
        case 'HomeScreen':
            return 'Home Page';
        case 'SwappedDevicesScreen':
            return 'Devices To Swap';
        case 'OrdersScreen':
            return 'My Orders';
    }
}
