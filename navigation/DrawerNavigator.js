// Main imports
import * as React from 'react';
import {
    createDrawerNavigator,
    DrawerContentScrollView,
    DrawerItemList,
    DrawerItem,
} from '@react-navigation/drawer';
import { DrawerActions } from '@react-navigation/native';
import TabBarIcon from '../components/UI/TabBarIcon';

// Screens imports
import SignInScreen from '../screens/User/SignInScreen';
import SignUpScreen from '../screens/User/SignUpScreen';
import SettingsScreen from '../screens/SettingsScreen';
import AboutScreen from '../screens/AboutScreen';
import BottomTabNavigator from './BottomTabNavigator';

// Navigation imports
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import CustomHeaderButton from '../components/UI/HeaderButton';
import { Platform, StyleSheet, Text, View, Image } from 'react-native';
import Colors from '../constants/Colors';
import { useDispatch, useSelector } from 'react-redux';
const Drawer = createDrawerNavigator();
const INITIAL_DRAWER_ROUTE_NAME = 'HomeScreen';

export default function DrawerNavigator({ navigation, route }) {
    navigation.setOptions({
        headerTitle: getHeaderTitle(route),
        headerLeft: () => (
            <HeaderButtons HeaderButtonComponent={CustomHeaderButton}>
                <Item title='Menu' iconName={Platform.OS === 'android' ? "md-menu" : "ios-menu"} onPress={() => { navigation.dispatch(DrawerActions.toggleDrawer()) }} />
            </HeaderButtons>
        ),
        headerRight: () => (
            <HeaderButtons HeaderButtonComponent={CustomHeaderButton}>
                <Item title='Menu' iconName={Platform.OS === 'android' ? "md-cart" : "ios-cart"} onPress={() => navigation.navigate('CartScreen')} />
            </HeaderButtons>
        ),
    });

    const [IsSignedIn, SetIsSignedIn] = React.useState(false);
    const isLogged = useSelector(State => State.userReducer.userData.isLoggedIn);
    const User = useSelector(State => State.userReducer.userData.User);
    
    console.log(User);
    function SwitchLanguage() {
        alert('قريباً...');
    }



    function CustomDrawerContent(props) {
        return (
            <DrawerContentScrollView {...props}>
                <View style={styles.drawerCustomHeader}>
                    {isLogged ?
                        <>
                            <Image style={styles.logoImage} source={require('../assets/images/user-profile-image.png')} />
                            <Text style={{ marginTop: 10 }}>Welcome back,</Text>
                            {User != null &&
                                <Text style={styles.userName}>{User.first_name} </Text>
                            }
                            
                        </> :
                        <>
                        {User != null &&
                            <Text style={styles.welcomeLabel}>WELCOME {User.first_name}  TO{"\n\n"}Kelshie</Text>
                        }
                        {User == null &&
                            <Text style={styles.welcomeLabel}>WELCOME TO{"\n\n"}Kelshie</Text>
                        }
                            </>
                    }
                </View>

                <DrawerItemList {...props} />

                <DrawerItem
                    label="English / العربية"
                    icon={({ focused }) => <TabBarIcon isDrawer={true} focused={focused} name="ios-switch" />}
                    onPress={() => SwitchLanguage()}
                />
            </DrawerContentScrollView>
        );
    }

    return (
        <Drawer.Navigator
            initialRouteName={INITIAL_DRAWER_ROUTE_NAME}
            drawerContent={props => <CustomDrawerContent {...props} />}
            drawerType='back'
            overlayColor="transparent"

        >
            <Drawer.Screen
                name="HomeScreen"
                component={BottomTabNavigator}

                options={{
                    drawerIcon: ({ focused }) => <TabBarIcon isDrawer={true} focused={focused} name="ios-home" />,
                    title: 'Home'
                }}
            />

            {!isLogged &&
                <>
                    <Drawer.Screen
                        name="SignInScreen"
                        component={SignInScreen}
                        options={{
                            drawerIcon: ({ focused }) => <TabBarIcon isDrawer={true} focused={focused} name="ios-log-in" />,
                            title: 'Sign In'
                        }}
                    />

                    <Drawer.Screen
                        name="SignUpScreen"
                        component={SignUpScreen}
                        options={{
                            drawerIcon: ({ focused }) => <TabBarIcon isDrawer={true} focused={focused} name="ios-person-add" />,
                            title: 'Sign Up'
                        }}
                    />
                </>
            }

            <Drawer.Screen
                name="SettingsScreen"
                component={SettingsScreen}
                options={{
                    drawerIcon: ({ focused }) => <TabBarIcon isDrawer={true} focused={focused} name="ios-settings" />,
                    title: 'Settings'
                }}
            />

            <Drawer.Screen
                name="AboutScreen"
                component={AboutScreen}
                options={{
                    drawerIcon: ({ focused }) => <TabBarIcon isDrawer={true} focused={focused} name="ios-information-circle" />,
                    title: 'About'
                }}
            />
        </Drawer.Navigator>
    );
}

function getHeaderTitle(route) {
    const routeName = route.state?.routes[route.state.index]?.name ?? INITIAL_DRAWER_ROUTE_NAME;

    switch (routeName) {
        case 'HomeScreen':
            return 'Home';
        case 'SignInScreen':
            return 'Sign In';
        case 'SignUpScreen':
            return 'Sign Up';
        case 'SettingsScreen':
            return 'Settings';
        case 'AboutScreen':
            return 'About Kelshie';
    }
}

const styles = StyleSheet.create({
    drawerCustomHeader: {
        backgroundColor: Colors.activeSelected,
        height: 150,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: -40,
        marginBottom: 9,

    },
    logoImage: {
        width: 55,
        height: 55,
        borderRadius: 50,
        backgroundColor: '#F8F4E3',
    },
    userName: {
        fontSize: 17,
        fontFamily: Platform.OS === 'ios' ? 'Arial' : 'monospace'
    },
    welcomeLabel: {
        fontSize: 20,
        fontWeight: 'bold',
        fontFamily: Platform.OS === 'ios' ? 'Arial' : 'monospace',
        textAlign: 'center'
    },
});