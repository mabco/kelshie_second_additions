// Main import
import * as Linking from 'expo-linking';

export default {
  prefixes: [Linking.makeUrl('/')],
  config: {
    BottomTabNavigator: {
      path: 'root',
      screens: {
        HomeScreen: 'home',
        CategoriesScreen: 'categories',
        ShopsScreen: 'shops',
        SwappedDevicesScreen: 'swapped',
        CartScreen: 'cart',
        ShopsScreenProduct:'shopsproduct',
        CategoriesScreenFromBrand:'CategoriesScreen'
      },
    },
    DrawerNavigator: {
      path: 'root',
      screens: {
        Home: 'home',
        Profile: 'profile',
        Settings: 'settings',
        About: 'about',
      },
    


    },
  },
};