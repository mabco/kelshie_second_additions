import Shop from "../../models/Shop";
import GetAPI_BaseURL from '../../constants/API/Connection';

export const GET_ALL_SHOPS = 'GET_ALL_SHOPS';
export const GET_SHOP_DETAILS = 'GET_SHOP_DETAILS';
export const GET_SHOP_PRODUCTS = 'GET_SHOP_PRODUCTS';


const API_BaseURL = GetAPI_BaseURL();

export const GetAllShops = () => {
    return async dispatch => {
        try {
            const Response = await fetch(API_BaseURL + 'shops', {
                headers: {
                    'Accept': 'application/json'
                }
            });

            if (!Response.ok) {
                throw new Error(Response.status + ': ' + Response.statusText);
            }

            const ResponseData = await Response.json();
            
            dispatch({
                type: GET_ALL_SHOPS, responseData: ResponseData
            });
        } catch (error) {
            console.log(error);
        }
    }
};

export const GetShopDetails = (ShopID) => {
    return async dispatch => {
        try {
            const Response = await fetch(API_BaseURL + 'getShopDetails/' + ShopID, {
                headers: {
                    'Accept': 'application/json'
                }
            });

            if (!Response.ok) {
                throw new Error(Response.status + ': ' + Response.statusText);
            }

            const ResponseData = await Response.json();

            dispatch({
                type: GET_SHOP_DETAILS, responseData: ResponseData
            });
        } catch (error) {
            console.log(error);
        }
    }
};

export const GetShopProducts = (ShopID) => {
    return async dispatch => {
        try {
            //console.log("action id ",ShopID);
            const Response = await fetch(API_BaseURL + 'getAvailableShopProducts/' + ShopID, {
                headers: {
                    'Accept': 'application/json'
                }
            });

            if (!Response.ok) {
                throw new Error(Response.status + ': ' + Response.statusText);
            }

            const ResponseData = await Response.json();
            // console.log("Action shop id",ShopID);
            // console.log("GetShopProducts",ResponseData);
            dispatch({
                type: GET_SHOP_PRODUCTS, responseData: ResponseData
            });
        } catch (error) {
            console.log(error);
        }
    }
};