export const ADD_TO_CART = 'ADD_TO_CART';
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART';
export const CLEAR_CART = 'CLEAR_CART';

export const AddToCart = Product => {
    console.log("llllppopopopopo", Product)
    return {type: ADD_TO_CART, product: Product};
};

export const RemoveFromCart = (ProductID) => {
    return {type: REMOVE_FROM_CART, productID: ProductID};
};

export const ClearCart = () => {
    return {type: CLEAR_CART};
};