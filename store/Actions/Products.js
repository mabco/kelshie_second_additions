import Shop from "../../models/Product";
import GetAPI_BaseURL from '../../constants/API/Connection';


export const GET_PRODUCT_DETAILS = 'GET_PRODUCT_DETAILS';
export const GET_PRODUCT_BY_CAT = 'GET_PRODUCT_BY_CAT';
export const GET_PRODUCT_BY_CAT_BRAND ='GET_PRODUCT_BY_CAT_BRAND';

const API_BaseURL = GetAPI_BaseURL();

export const GetProductDetails = (ProductID) => {
    return async dispatch => {
        try {
            const Response = await fetch(API_BaseURL + 'getProductDetails/'+ProductID, {
                headers: {
                    'Accept': 'application/json'
                }
            });

            if (!Response.ok) {
                throw new Error(Response.status + ': ' + Response.statusText);
            }

            const ResponseData = await Response.json();
            dispatch({
                type: GET_PRODUCT_DETAILS, responseData: ResponseData
            });
        } catch (error) {
            console.log(error);
        }
    }
};


export const GetProductByCat = (CatID) => {
    return async dispatch => {
        try {
            const Response = await fetch(API_BaseURL + 'specSearch?categori_id='+CatID, {
                headers: {
                    'Accept': 'application/json'
                }
            });
            // console.log("get productttttttttttttt  ",CatID);
            if (!Response.ok) {
                throw new Error(Response.status + ': ' + Response.statusText);
            }

            const ResponseData = await Response.json();
            // console.log("get product by cat ",ResponseData);
            dispatch({
                type: GET_PRODUCT_BY_CAT, responseData: ResponseData
            });
        } catch (error) {
            console.log(error);
        }
    }
};


export const GetProductByCatBrand = (CatID,BrandID) => {
    return async dispatch => {
        try {
            const Response = await fetch(API_BaseURL + 'specSearch?brand_id='+BrandID+'&categori_id='+CatID, {
                headers: {
                    'Accept': 'application/json'
                }
            });
            if (!Response.ok) {
                throw new Error(Response.status + ': ' + Response.statusText);
            }

            const ResponseData = await Response.json();
            //   console.log("get product by cat Brand  ",ResponseData);
            dispatch({
                type: GET_PRODUCT_BY_CAT_BRAND, responseData: ResponseData
            });
        } catch (error) {
            console.log(error);
        }
    }
};




