import Brand from "../../models/Brand";
import GetAPI_BaseURL from '../../constants/API/Connection';

export const GET_BRANDS = 'GET_BRANDS';

const API_BaseURL = GetAPI_BaseURL();

export const GetBrands = () => {
    return async dispatch => {
        try {
            const Response = await fetch(API_BaseURL + 'brands', {
                headers: {
                    'Accept': 'application/json'
                }
            });

            if (!Response.ok) {
                throw new Error(Response.status + ': ' + Response.statusText);
            }

            const ResponseData = await Response.json();
            const Brands = [];

            for (const Key in ResponseData) {
                Brands.push(new Brand(
                    ResponseData[Key].id,
                    ResponseData[Key].name,
                    ResponseData[Key].icon
                ));
            }            

            dispatch({
                type: GET_BRANDS, responseData: Brands
            });
        } catch (error) {
            console.log(error);
        }
    }
};