import Category from "../../models/Category";
import GetAPI_BaseURL from '../../constants/API/Connection';

export const GET_CATEGORIES = 'GET_CATEGORIES';

const API_BaseURL = GetAPI_BaseURL();

export const GetCategories = () => {
    return async dispatch => {
        try {
            const Response = await fetch(API_BaseURL + 'categories', {
                headers: {
                    'Accept': 'application/json'
                }
            });

            if (!Response.ok) {
                throw new Error(Response.status + ': ' + Response.statusText);
            }

            const ResponseData = await Response.json();
            const Categories = [];

            for (const Key in ResponseData) {
                Categories.push(new Category(
                    ResponseData[Key].id,
                    ResponseData[Key].name,
                    ResponseData[Key].icon
                ));
            }

            dispatch({
                type: GET_CATEGORIES, responseData: Categories
            });
        } catch (error) {
            console.log(error);
        }
    }
};