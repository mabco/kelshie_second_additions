export const ADD_ORDER = 'ADD_ORDER';
export const CANCEL_ORDER = 'CANCEL_ORDER';
import GetAPI_BaseURL from '../../constants/API/Connection';


const API_BaseURL = GetAPI_BaseURL();

export const SEND_ORDER = 'SEND_ORDER';
export const AddOrder = (CartItems, TotalAmount) => {
    return { type: ADD_ORDER, order: { items: CartItems, amount: TotalAmount } };
};

export const CancelOrder = (OrderID) => {
    return { type: CANCEL_ORDER, orderID: OrderID };
};
export const SendOrder  = (token, shop_product_array) => {
    return async dispatch => {
        try {
            var RequestData = new FormData();
            items=[]
            shop_product_array.forEach(element => {
                items.append(shop_product_id=shop_product_array.shop_product_id,note=shop_product_array.note)
            });
            RequestData.append("shopProducts", items);
            

            const Response = await fetch(API_BaseURL + 'orderShopProduct', {
                method: 'POST',
                headers: {
                    'Authorization': 'Bearer Token '+token, 
                    'Accept': 'application/json'
                },
                body: RequestData
            });
            console.log(RequestData);
            if (!Response.ok) {
                throw new Error(Response.status + ': ' + Response.statusText);

            }

            const ResponseData = await Response.json();
            console.log("register responce Action ",ResponseData);
            dispatch({
                type: SEND_ORDER, responseData: ResponseData
            });
        } catch (error) {
            console.log("send order error",error);
        }
    }
};
