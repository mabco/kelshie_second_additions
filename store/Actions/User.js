import User from "../../models/User";
import GetAPI_BaseURL from '../../constants/API/Connection';

const API_BaseURL = GetAPI_BaseURL();

export const REGISTER = 'REGISTER';
export const SEND_VERIFICATION_CODE = 'SEND_VERIFICATION_CODE';
export const SIGN_IN = 'SIGN_IN';
export const REGION_LIST = 'REGION_LIST';
export const REGISTERERROR = 'REGISTERERROR';
export const SIGN_INERROR = 'SIGN_INERROR';
export const REGISTERERRORNUMBER = 'REGISTERERRORNUMBER';
export const SENDVCODEERROR = 'SENDVCODEERROR';




export const Register = (FirstName, LastName, BirthDay, MobileNumber, RegionID, AddressDetails, EmailAddress = null) => {
    return async dispatch => {
        try {
            var RequestData = new FormData();
            RequestData.append("first_name", FirstName);
            RequestData.append("last_name", LastName);
            RequestData.append("birthday", BirthDay);
            RequestData.append("phone", MobileNumber);
            RequestData.append("region_id", RegionID);
            RequestData.append("address_details", AddressDetails);
            RequestData.append("email_address", EmailAddress);

            const Response = await fetch(API_BaseURL + 'register', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json'
                },
                body: RequestData
            });
            console.log(RequestData);
            if (!Response.ok) {
                throw new Error(Response.status + ': ' + Response.statusText);

            }

            const ResponseData = await Response.json();
            console.log("register responce Action ",ResponseData);
            dispatch({
                type: REGISTER, responseData: ResponseData
            });
        } catch (error) {
            console.log("register error",error);
                dispatch({
                    type: REGISTERERROR
                });
          

        }
    }
};

export const SendVerificationCode = (MobileNumber) => {
    return async dispatch => {
        try {
            var RequestData = new FormData();
            RequestData.append("phone", MobileNumber);

            const Response = await fetch(API_BaseURL + 'sendVerificationCode', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json'
                },
                body: RequestData
            });

            if (!Response.ok) {
                
                throw new Error(Response.status + ': ' + Response.statusText);
            }

            const ResponseData = await Response.json();
            console.log("Send v code",ResponseData);
            dispatch({
                type: SEND_VERIFICATION_CODE, responseData: ResponseData
            });

        } catch (error) {
            console.log(error);
            dispatch({
                type: SENDVCODEERROR
            });
        }
    }
};

export const SignIn = (MobileNumber, VerificationCode) => {
    return async dispatch => {
        try {
            var RequestData = new FormData();
            RequestData.append("phone", MobileNumber);
            RequestData.append("verification_code", VerificationCode);

            const Response = await fetch(API_BaseURL + 'login', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json'
                },
                body: RequestData
            });
            console.log("body",RequestData);
            if (!Response.ok) {
                
                 throw new Error(Response.status + ': ' + Response.statusText);
            }

            const ResponseData = await Response.json();
            // console.log("sign IN responce", ResponseData);
            dispatch({
                type: SIGN_IN, responseData: ResponseData
            });
        } catch (error) {
            console.log("sign In error", error);
                dispatch({
                    type: SIGN_INERROR
                });
        }
    }
};




export const ReginList = () => {
    return async dispatch => {
        try {

            const Response = await fetch(API_BaseURL + 'regions', {
                method: 'GET',
                headers: {
                    'Accept': 'application/json'
                },

            });

            if (!Response.ok) {
                throw new Error(Response.status + ': ' + Response.statusText);
            }

            const ResponseData = await Response.json();
            // console.log("rahaf", ResponseData);

            dispatch({
                type: REGION_LIST, responseData: ResponseData
            });
        } catch (error) {
            console.log(error);

        }
    }
};



