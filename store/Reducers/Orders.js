import { ADD_ORDER, CANCEL_ORDER,SEND_ORDER } from "../Actions/Orders";
import Order from "../../models/Order";

const InitialState = {
    orders: [],
    send_orders:null,
};

export default (State = InitialState, Action) => {
    switch (Action.type) {
        case ADD_ORDER:
            const NewOrder = new Order(
                new Date().toString(),
                Action.order.items,
                Action.order.amount,
                new Date()
            );

            return {
                // ...State,
                orders: State.orders.concat(NewOrder)
            };
        case CANCEL_ORDER:

        case SEND_ORDER:
            let ResponseData = Action.responseData;
            

            send_orders = ResponseData;


            console.log('Order Reducer: ', ResponseData);
            break;
            
    }

    return State;
};