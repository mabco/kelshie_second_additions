import { ADD_TO_CART, REMOVE_FROM_CART, CLEAR_CART } from "../Actions/Cart";
import { ADD_ORDER } from "../Actions/Orders";
import CartItem from "../../models/CartItem";

const InitialState = {
    items: {},
    totalAmount: 0,
    isAdded:false,
};

export default (State = InitialState, Action) => {
    switch (Action.type) {
        case ADD_TO_CART:   
            const AddedProduct = Action.product;
            const ProductName = AddedProduct.name;
            console.log("state update",AddedProduct);
            const ProductPrice = AddedProduct.shop_products[0].price;
            let NewOrUpdatedCartItem;
            let Added=false
            if (State.items[AddedProduct.id]) {
                Added=true;
                // NewOrUpdatedCartItem = new CartItem(
                //    // State.items[AddedProduct.id].quantity + 1,
                //     ProductName,
                //     ProductPrice,
                //     State.items[AddedProduct.id].sum + ProductPrice);
            } else {
                NewOrUpdatedCartItem = new CartItem(1, ProductName, ProductPrice, ProductPrice);
            }
            
            return {
                // ...State,
                
                items: { ...State.items, [AddedProduct.id]: NewOrUpdatedCartItem },
                totalAmount: State.totalAmount + ProductPrice
            };
        case REMOVE_FROM_CART:
            const SelectedCartItem = State.items[Action.productID];
            const CurrentQuantity = SelectedCartItem.quantity;
            let UpdatedCartItems;

            if (CurrentQuantity > 1) {
                const UpdatedCartItem = new CartItem(
                    // SelectedCartItem.quantity - 1,
                    SelectedCartItem.title,
                    SelectedCartItem.price,
                    SelectedCartItem.sum - SelectedCartItem.price);
                UpdatedCartItems = { ...State.items, [Action.productID]: UpdatedCartItem };
            } else {
                UpdatedCartItems = { ...State.items };
                delete UpdatedCartItems[Action.productID];
            }
            
            return {
                ...State,
                items: UpdatedCartItems,
                totalAmount: State.totalAmount - SelectedCartItem.price
            };
        case CLEAR_CART:
        case ADD_ORDER:
            return InitialState;
    }

    return State;
};