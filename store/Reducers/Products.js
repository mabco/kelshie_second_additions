
import { GET_PRODUCT_DETAILS ,GET_PRODUCT_BY_CAT,GET_PRODUCT_BY_CAT_BRAND} from "../Actions/Products";

const InitialState = {
    //availableProducts: PRODUCTS,
    ProductSpecs: [],
    ProductByCat: [],
    ProductByCatBrand:[],
    

};

export default (State = InitialState, Action) => {
    switch (Action.type) {
        case GET_PRODUCT_DETAILS:
            const ResponseData = Action.responseData;
            // console.log("ggggggggggggg",Action.responseData);
            return {
                // ...State,
                ProductSpecs:  ResponseData,

                
            };

        case GET_PRODUCT_BY_CAT:
            const ResponseDataCat = Action.responseData;
            return {
                // ...State,
                ProductByCat: ResponseDataCat,
               

                
            };

        case GET_PRODUCT_BY_CAT_BRAND:
            const ResponseDataCatBrand = Action.responseData;
            console.log("log",ResponseDataCatBrand);
            return {
                // ...State,
                
                ProductByCatBrand: ResponseDataCatBrand,
               

                
            };


        };
    return State;
     


}