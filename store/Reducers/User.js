import { REGISTER, SEND_VERIFICATION_CODE, SIGN_IN, REGION_LIST, REGISTERERROR, REGISTERERRORNUMBER ,SIGN_INERROR,SENDVCODEERROR} from "../Actions/User";
import User from '../../models/User'
const InitialState = {
    userData: {
        User: null,
        isLoggedIn: false,
        userObject: null,
        isError: false,
        isRegister: false,
        allRegion: [],
        isErrorRegister: false,
        isVcodeErroe:false,
        IsVcodeOK:false,
    },
};

export default (State = InitialState, Action) => {
    let NewUser=null;
    switch (Action.type) {
        case REGISTER:
            let RegisterResponseData = Action.responseData;
            State.userData = RegisterResponseData;

            State.userData.isRegister = true;


            console.log('REGISTER Reducer: ', RegisterResponseData);
            break;
        case SEND_VERIFICATION_CODE:
            let SendVerificationCodeResponseData = Action.responseData;

            State.userData = SendVerificationCodeResponseData;
            State.userData.IsVcodeOK=true;
            // console.log('SEND_VERIFICATION_CODE: ', SendVerificationCodeResponseData);


            // console.log('SEND_VERIFICATION_CODE: ', SendVerificationCodeResponseData);
            break;
        case SIGN_IN:
            let SignInResponseData = Action.responseData;
            console.log(SignInResponseData)
            NewUser = new User(
                SignInResponseData.id,
                SignInResponseData.first_name,
                SignInResponseData.last_name,
                SignInResponseData.mobile_number,
                SignInResponseData.username,
                SignInResponseData.email_address,
                SignInResponseData.verification_code,
                SignInResponseData.verification_code_expire_date,
                // RegionResponseData.wallet,
                SignInResponseData.access_token,
            );
            State.userData.User=NewUser;
            // if (SignInResponseData.hasOwnProperty('type')) {
                
            // }
            State.userData.isLoggedIn= true;
            // console.log('LOGIN: ', SignInResponseData);
            break;


        case REGION_LIST:
            let RegionResponseData = Action.responseData;
            State.allRegion = Action.responseData;

            break;

        case REGISTERERROR:
            State.userData.isErrorRegister = true;
            break;


        case REGISTERERRORNUMBER:
            State.userData.isErrorRegisterNumber = true;
            break;
            
        case SIGN_INERROR:
            State.userData.isError = true;
            break;

        case SENDVCODEERROR:
            State.userData.isVcodeErroe=true;

        
        break;
    }
    return State;
};