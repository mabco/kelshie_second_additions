import { GET_CATEGORIES } from "../Actions/Categories";

const InitialState = {
    allCategories: [],
};

export default (State = InitialState, Action) => {
    switch (Action.type) {
        case GET_CATEGORIES:
            State.allCategories = Action.responseData;
        break;
    }

    return State;
};