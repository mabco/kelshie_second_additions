import { GET_ALL_SHOPS, GET_SHOP_DETAILS, GET_SHOP_PRODUCTS } from "../Actions/Shops";
import Shop from "../../models/Shop";

const InitialState = {
    allShops: {}
};

export default (State = InitialState, Action) => {
    let NewShop = null;
    let AllUpdatedShops = State.allShops;

    switch (Action.type) {
        case GET_ALL_SHOPS:
            const ResponseData = Action.responseData;
            
            let NewShop = null;

            for (const Key in ResponseData) {
                let ShopID = ResponseData[Key].id;

                if (!State.allShops[ShopID]) {
                    NewShop = new Shop(
                        ResponseData[Key].id,
                        ResponseData[Key].name,
                        ResponseData[Key].logo_image,
                        ResponseData[Key].cover_image,
                        ResponseData[Key].closed_time,
                        ResponseData[Key].avg_rating
                    );

                    AllUpdatedShops = { ...AllUpdatedShops, [NewShop.id]: NewShop };
                }
            }

            return {
                // ...State,
                allShops: AllUpdatedShops,
            };
        case GET_SHOP_DETAILS:
            const SelectShopDetails = Action.responseData;
            const ShopAddress = SelectShopDetails.address;
            const ShopReviews = SelectShopDetails.reviews;

            if (!State.allShops[SelectShopDetails.id]) {
                NewShop = new Shop(
                    SelectShopDetails.id,
                    SelectShopDetails.name,
                    SelectShopDetails.logo_image,
                    SelectShopDetails.cover_image,
                    SelectShopDetails.closed_time,
                    SelectShopDetails.avg_rating,
                    SelectShopDetails.address,
                    null,
                    SelectShopDetails.reviews
                );

                AllUpdatedShops = { ...State.allShops, [SelectShopDetails.id]: NewShop };
            } else {
                AllUpdatedShops[SelectShopDetails.id].address = ShopAddress;
                AllUpdatedShops[SelectShopDetails.id].reviews = ShopReviews;
            }
            
            return {
                // ...State,
                allShops: AllUpdatedShops,
            };
        case GET_SHOP_PRODUCTS:
            const ShopProducts = Action.responseData;
            // console.log("Reducer ShopProducts",ShopProducts);
            if (ShopProducts.length > 0) {
                //console.log("Reducer shop id",ShopProducts[0].shop_products[0].shop_id);
                const ShopID = ShopProducts[0].shop_products[0].shop_id;
                if (State.allShops[ShopID]) {
                    AllUpdatedShops[ShopID].products = ShopProducts;
                }
                // console.log("Reducer shop id",ShopID);
                // console.log("GET_SHOP_PRODUCTS",ShopProducts);
            }

            return {
                // ...State,
                allShops: AllUpdatedShops,
            };
    }

    return State;
};