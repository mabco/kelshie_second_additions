import { GET_BRANDS } from "../Actions/Brands";

const InitialState = {
    allBrands: [],
};

export default (State = InitialState, Action) => {
    switch (Action.type) {
        case GET_BRANDS:
            State.allBrands = Action.responseData;
        break;
    }

    return State;
};