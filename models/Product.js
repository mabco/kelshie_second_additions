class Product {
    constructor(id, brand_id, category_id, name,brand,category, shops_products, images) {
        this.id = id;
        this.brand_id = brand_id;
        this.category_id = category_id;
        this.name = name;
        this.brand=brand;
        this.category=category;
        this.shops_products = shops_products;
        this.images = images;
    }
}

export default Product;