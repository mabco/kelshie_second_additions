class Customer {
    constructor(id, first_name, last_name, mobile_number, username, email_address, verification_code, verification_code_expire_date, access_token) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.mobile_number = mobile_number;
        this.username = username;
        // this.password = password;
        this.email_address = email_address;
        this.verification_code = verification_code;
        this.verification_code_expire_date = verification_code_expire_date;
        // this.wallet = wallet;
        this.access_token=access_token;
        // this.is_active = is_active;
        // this.is_locked = is_locked;
        // this.is_information_set = is_information_set;
        // this.address_id = address_id;
    }
}

export default Customer;