class Shop {
    constructor(id, name, logo_image, cover_image, closed_time, avg_rating, address = null, products = null, reviews = null) {
        this.id = id;
        this.name = name;
        this.logo_image = logo_image;
        this.cover_image = cover_image;
        this.closed_time = closed_time;
        this.avg_rating = avg_rating;
        this.address = address;
        this.products = products;
        this.reviews = reviews;
    }
}

export default Shop;