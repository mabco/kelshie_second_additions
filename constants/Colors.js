const tintColor = '#14213D';

export default {
  tintColor,
  tabIconDefault: '#fff',
  activeSelected: '#F0E210',
  tabBar: '#fefefe',
  errorBackground: 'red',
  errorText: '#fff',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: tintColor,
  noticeText: '#fff',
  pricetext: '#3C64B9',
  Producttitle: '#3C64B9',
  backgroundColor: '#fafafa',
};
