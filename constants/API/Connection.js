const API_REQUEST = 'http://';
const API_IP = '82.165.100.137';
const API_PORT = ':8000';
const API_ROOT_Name = 'api';

export default function GetAPI_BaseURL() {
    return API_REQUEST + API_IP + API_PORT + `/${API_ROOT_Name}/`;
}