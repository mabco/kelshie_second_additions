import * as React from 'react';
import { StyleSheet,} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import Colors from '../constants/Colors';
import  {CustomText }  from '../components/UI/StyledText';

export default function AboutScreen() {
    return (
        <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
            <CustomText isArabic={false}>Here will show details about our App.</CustomText>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.backgroundColor,
    },
    contentContainer: {
        paddingTop: 15,
        paddingLeft: 15
    },
});