import React, { useState, useEffect } from 'react';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { View, Dimensions, StyleSheet, TouchableHighlight, TouchableOpacity, ScrollView, Image, Platform, FlatList,Text } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import Colors from '../constants/Colors';
import * as CategoriesActions from '../store/Actions/Categories';
import * as BrandsActions from '../store/Actions/Brands';
import BrandItem from '../components/Products/BrandItem';
import { CustomText } from '../../react/components/UI/StyledText';
const { width, height } = Dimensions.get('window');
const SCREEN_WIDTH = width < height ? width : height;
const ProductNumColumns = 2;
const Product_ITEM_HEIGHT = 150;
const Product_ITEM_MARGIN = 10;
const { width: screenWidth } = Dimensions.get('window');
const { width: viewportWidth } = Dimensions.get('window');

const ENTRIES1 = [
    {
        illustration: 'https://i.imgur.com/UPrs1EWl.jpg',
    },
    {
        illustration: 'https://i.imgur.com/UPrs1EWl.jpg',
    },
    {
        illustration: 'https://i.imgur.com/MABUbpDl.jpg',
    },
    {
        illustration: 'https://i.imgur.com/KZsmUi2l.jpg',
    },
    {
        illustration: 'https://i.imgur.com/2nCt3Sbl.jpg',
    },
];

export default function HomeScreen({ navigation }) {
    let [CurrentHomeSlider, SetCurrentHomeSlider] = useState(0);
    const Products = useSelector(State => State.productsReducer.ProductSpecs);
    const Brands = useSelector(State => State.brandsReducer.allBrands);

    const [Errors, SetErrors] = React.useState([]);
    const Dispatch = useDispatch();

    const LoadCategories = React.useCallback(async () => {
        try {
            await Dispatch(CategoriesActions.GetCategories());
        } catch (error) {
            SetErrors([...Errors, error.message])
        }
    }, [Dispatch, SetErrors]);

    const LoadBrands = React.useCallback(async () => {
        try {
            await Dispatch(BrandsActions.GetBrands());
        } catch (error) {
            SetErrors([...Errors, error.message])
        }
    }, [Dispatch, SetErrors]);

    useEffect(() => {
        SetErrors([]);
        LoadCategories();
        LoadBrands();
    }, [LoadCategories, LoadBrands, SetErrors]);
    function shuffle(array) {
        var currentIndex = array.length, temporaryValue, randomIndex;
      
        // While there remain elements to shuffle...
        while (0 !== currentIndex) {
      
          // Pick a remaining element...
          randomIndex = Math.floor(Math.random() * currentIndex);
          currentIndex -= 1;
      
          // And swap it with the current element.
          temporaryValue = array[currentIndex];
          array[currentIndex] = array[randomIndex];
          array[randomIndex] = temporaryValue;
        }
      
        return array;
      }
    const renderItem = ({ item, index }, parallaxProps) => {
        return (
            <TouchableOpacity>
                <View style={styles.imageContainer}>
                    <Image style={styles.image} source={{ uri: item.illustration }} />
                </View>
            </TouchableOpacity>
        );
    };

    return (
        <ScrollView>
            <View style={styles.container}>
                <View style={styles.carouselcontainer}>
                    <Carousel
                        data={ENTRIES1}
                        renderItem={renderItem}
                        sliderWidth={viewportWidth}
                        itemWidth={viewportWidth}
                        firstItem={0}
                        autoplay={true}
                        isLooped={true}
                        loop={true}
                        autoplayDelay={500}
                        autoplayInterval={3000}
                        onSnapToItem={index => SetCurrentHomeSlider(index)}
                    />

                    <Pagination
                        dotsLength={ENTRIES1.length}
                        containerStyle={styles.paginationContainer}
                        dotColor="#FCA311"
                        dotStyle={styles.paginationDot}
                        inactiveDotColor="#FFFFFF"Brands
                        inactiveDotOpacity={0.4}
                        inactiveDotScale={0.6}
                        activeDotIndex={CurrentHomeSlider}
                    />
                </View>
            </View>

            <FlatList
                style={styles.container}
                data={shuffle(Brands)}
                horizontal={true}
                keyExtractor={(item) => {
                    return item.id.toString();
                }}
                ItemSeparatorComponent={() => {
                    return (
                        <View style={styles.separator} />
                    )
                }}
                renderItem={(post) => {
                    const Item = post.item;

                    return (
                        <BrandItem
                            name={Item.name}
                            image={Item.icon}
                            // navigation={navigation}
                            // BrandID={Item.id}
                            onBrandPress={() => navigation.navigate('CategoriesScreenFromBrand', { BrandID: Item.id ,title:Item.name })}
                        />
                    )
                }}
            />

            <View style={styles.container}>
            <TouchableOpacity onPress={() => navigation.navigate('ShopsScreen')}>
            <View style={styles.card}>
                <Image style={styles.cardImage} source={require('../../react/assets/images/login.png')} />
                
                <View style={styles.cardContent}>
                    <View>
                    <CustomText isArabic={true} style={styles.name}>Kelshie Shops</CustomText>
                    </View>
                </View>
            </View>
        </TouchableOpacity>

            </View>
             


        </ScrollView >
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    carouselcontainer: {
        height: 197,
        backgroundColor: '#00000033',
    },
    item: {
        width: screenWidth - 60,
        height: screenWidth - 150,
    },
    ProductContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: Product_ITEM_MARGIN,
        marginTop: 20,
        width: (SCREEN_WIDTH - (ProductNumColumns + 10) * Product_ITEM_MARGIN) / ProductNumColumns,
        height: Product_ITEM_HEIGHT + 75,
        borderColor: '#FCA311',
        borderWidth: 0.5,
        borderRadius: 20
    },
    imageContainer: {
        flex: 1,
        justifyContent: 'center',
        width: viewportWidth,
        height: 250
    },
    image: {
        ...StyleSheet.absoluteFillObject,
        width: '100%',
        height: 200
    },
    paginationContainer: {
        flex: 1,
        position: 'absolute',
        alignSelf: 'center',
        paddingVertical: 8,
        marginTop: 170,
    },
    paginationDot: {
        width: 8,
        height: 8,
        borderRadius: 4,
        marginHorizontal: 0
    },
    category: {
        marginTop: 5,
        marginBottom: 5
    },
    separator: {
        marginTop: 3,
    },

    card: {
        margin: 0,
        borderRadius: 2,
        borderWidth: 1,
        borderColor: "#DCDCDC",
        backgroundColor: "#DCDCDC",
    },
    cardImage: {
        flex: 1,
        height: 250,
        width: null,
    },
    cardContent: {
        paddingVertical: 12.5,
        paddingHorizontal: 16,
        flex: 1,
        display: 'flex',
        alignContent: 'center',
        height: 200,
        width: null,
        position: 'absolute',
        zIndex: 100,
        left: 0,
        right: 0,
        backgroundColor: 'transparent'
    },
    name: {
        fontSize: 30,
        color: "black",
        marginTop: 10,
        fontWeight: 'bold',
        marginTop:175

        // backgroundColor:Colors.activeSelected,
        // borderRadius:30
    },
});
