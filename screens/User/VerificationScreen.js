import React, { useState, useEffect, useCallback } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableOpacity,
    Image,
    Alert,Dimensions
} from 'react-native';

import * as UserActions from '../../store/Actions/User';
import { useDispatch, useSelector } from 'react-redux';
import APIKit from '../User/APIKit.js';
import { Formik } from 'formik';
import * as Yup from 'yup';
import Colors from '../../constants/Colors';
const FormValidationSchema = Yup.object({
    verificationCode: Yup
        .string().required("The verification code is required!")
        .min(5, 'The verification code is invalid!')
});
const { width: screenWidth } = Dimensions.get('window');

export default function VerificationScreen({ navigation, route }) {
    navigation.setOptions({
        headerTitle: 'Account Verification'
    });

    const MobilePhoneNumber = route.params.mobilePhoneNumber;
    const isLogged = useSelector(State => State.userReducer.userData.isLoggedIn);
    let validationError = useSelector(State => State.userReducer.userData.isError);
    const [IsError, SetIsError] = useState(false);
    const Dispatch = useDispatch();
    // Dispatch(UserActions.SignIn(MobilePhoneNumber,values)); // Temporary commented




    const SignIn = useCallback(async (values) => {
        try {
            await Dispatch(UserActions.SignIn(MobilePhoneNumber, values));
        } catch (error) {
        }


    }, [Dispatch]);
    useEffect(() => {
        SignIn();
    }, [Dispatch, SignIn]);

    // console.log("validationError", validationError)
    console.log("isLogged", isLogged)
    // function onFailure(error) {
        
    // }
    const onFailure = (error) => {
        Alert.alert(
            'expired verification code ,, please resend it again',
            '', // <- this part is optional, you can pass an empty string
            [
                { text: 'OK', onPress: () =>  navigation.navigate('DrawerNavigator', { screen: 'SignInScreen' }) },
            ],
            // { cancelable: false },
        );
        
       
      };
    // if (isLogged) {
    //     navigation.navigate('DrawerNavigator', { screen: 'HomeScreen' })
    // }
    return (
        <View style={styles.container}>
            <Image style={styles.bgImage} source={require('../../assets/images/login.png')} />

            <Text style={styles.headerTitle}>Welcome</Text>
            <Text style={styles.verificationCodeMessage}>We sent you a verification code, please enter it in the field down below</Text>

            <Formik
                initialValues={{ verificationCode: '' }}
                validationSchema={FormValidationSchema}
                onSubmit={(values, actions) => {
                    // Dispatch(UserActions.SignIn(MobilePhoneNumber, verificationCode));
                     SignIn(values.verificationCode);
                    APIKit.post('http://82.165.100.137:8000/api/login?phone='+MobilePhoneNumber+'&verification_code='+values.verificationCode )
                    .then(response => {
                        console.log("ooooookkkkkkkkkkk",response)
                        if (response.status === 200) {
                         navigation.navigate('DrawerNavigator', { screen: 'HomeScreen' })
                    }
                    // break;
                    }).catch(error => {
                        if(error!=null){
                            console.log("noooooooooooot",error)
                             onFailure()
                        }
                        
                    });
                    actions.resetForm();
                }}
            >
                {({ handleChange, handleBlur, handleSubmit, values, errors, touched }) => (
                    <View style={styles.card}>
                        <View style={styles.inputContainer}>
                            <TextInput
                                style={styles.inputs}
                                keyboardType='numeric'
                                placeholder="Verification Code"
                                underlineColorAndroid='transparent'
                                onChangeText={handleChange('verificationCode')}
                                onBlur={handleBlur('verificationCode')}
                                value={values.verificationCode}
                            />

                            <Image style={styles.inputIcon} source={require('../../assets/images/icons/verification-code.png')} />
                        </View>

                        {touched.verificationCode && errors.verificationCode && <Text style={styles.error}>{errors.verificationCode}</Text>}

                        <TouchableOpacity style={[styles.buttonContainer, styles.loginButton]} onPress={handleSubmit}>
                            <Text style={styles.loginText}>Verify</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.buttonContainertext} onPress={() => Alert.alert("Alert", "Resend code")}>
                <Text style={styles.btnText}>Didn't receive a code? Resend Code</Text>
            </TouchableOpacity>
                    </View>
                )}
            </Formik>

           
        </View>
    );
}

const resizeMode = 'cover';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#DCDCDC',
    },
    inputContainer: {
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        width: screenWidth-150,
        height: screenWidth-500,
        marginTop: 10,
        flexDirection: 'row',
        alignItems: 'center',
        shadowColor: "#808080",
        shadowRadius: 5,
        elevation: 6,
        
        
    },
    inputs: {
        height: 45,
        marginLeft: 16,
        paddingRight: 10,
        borderBottomColor: '#FFFFFF',
        flex: 1,
    },
    inputIcon: {
        width: 25,
        height: 25,
        marginRight: 15,
        justifyContent: 'center'
    },
    buttonContainer: {
        height: 45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20,
        width: screenWidth-150,
        borderRadius: 30,
        backgroundColor: 'transparent'
    },
    buttonContainertext: {
        height: 45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        
        marginTop: 10,
        width: 300,
        borderRadius: 30,
        backgroundColor: 'transparent'
    },
    loginButton: {
        backgroundColor: "#000000",
        shadowColor: "#808080",
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0,
        shadowRadius: 3,
        elevation: 15,
    },
    loginText: {
        color: 'white',
    },
    bgImage: {
        flex: 1,
        resizeMode,
        position: 'absolute',
        width: '100%',
        height: '100%',
        justifyContent: 'center',
    },
    btnText: {
        color: "white",
        fontWeight: 'bold'
    },
    error: {
        marginTop: 15,
        color: 'white',
        fontWeight: 'bold',
        alignSelf:"auto",
        // alignItems:"stretch",
        
    },
    headerTitle: {
        fontSize: 35,
        color: '#FFF',
        position: 'relative',
        top: -40,
        textShadowColor: '#ccc',
        textShadowRadius: 15,
    },
    verificationCodeMessage: {
        paddingHorizontal: 10,
        marginBottom: 20,
        width: 300,
        color: '#FFF',
        fontWeight: 'bold',
        textShadowColor: '#000',
        textShadowRadius: 4,
        textAlign: 'center'
    },
    card: {
        backgroundColor: Colors.activeSelected,
         width: screenWidth-100,
        height: screenWidth-100,
        // flex: 1,
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,
        elevation: 24,
        shadowColor: "#000000",
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius:10
        
        
      }
}); 