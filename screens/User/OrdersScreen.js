import React, { useState, useEffect, useCallback } from 'react';
import { StyleSheet, FlatList, Text, View, Button,TouchableOpacity, } from 'react-native';

import { useDispatch, useSelector } from 'react-redux';
import OrderItem from '../../components/Shop/OrderItem';
import { CustomText } from '../../components/UI/StyledText';
import * as OrderActions from '../../store/Actions/Orders'; 
export default function OrdersScreen({ navigation }) {
    navigation.setOptions({
        headerTitle: 'My Orders'
    });
    const User = useSelector(State => State.userReducer.userData.User);
    const Orders = useSelector(State => State.ordersReducer.orders);
    const Dispatch = useDispatch();
    if (Orders.length == 0) {
        return (
            <View>
                <CustomText style={styles.comingSoon} isArabic={true}>  لا يوجد منتجات ! </CustomText>
                {/* <Button style={styles.loginButton} title="Back" onPress={() => navigation.navigate('BottomTabNavigator', { screen: 'ShopsScreen' })} /> */}


                <TouchableOpacity style={[styles.buttonContainer, styles.loginButton]} onPress={() => navigation.navigate('BottomTabNavigator', { screen: 'ShopsScreen' })}>
                    <Text style={styles.loginText}>Back</Text>
                </TouchableOpacity>

            </View>)



    }
    values=[
        {shop_product_id:167,note:"note note ..."},
        {shop_product_id:497,note:"note note ..."},
        {shop_product_id:552,note:"note note ..."},
        {shop_product_id:141,note:"note note ..."}
        
    ]
    const SendOrders = useCallback(async () => {
        try {
            await Dispatch(OrderActions.SendOrder(User.access_token, values));
        } catch (error) {
        }


    }, [Dispatch]);
    useEffect(() => {
        SendOrders();
    }, [Dispatch, SendOrders]);
    return (
        <FlatList
            data={Orders}
            style={styles.ordersList}
            keyExtractor={Item => Item.id}
            renderItem={Item => (
                <OrderItem
                    amount={Item.item.amount}
                    date={Item.item.GetReadableDate}
                    items={Item.item.items}
                />
            )}
        />
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fafafa',
    },
    contentContainer: {
        paddingTop: 15,
        paddingLeft: 15
    },
    ordersList: {
        marginTop: 20
    },
    comingSoon: {
        fontSize: 40,
        textAlign: 'center',
        marginTop: 30,
    },
    buttonContainer: {
        height: 45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20,
        marginTop: 10,
        borderRadius: 30,
        backgroundColor: 'transparent'
    },
    loginButton: {
        backgroundColor: "#00b5ff",
        shadowColor: "#808080",
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0,
        shadowRadius: 3,
        elevation: 15,
    },
    loginText: {
        color: 'white',
    },
   
});