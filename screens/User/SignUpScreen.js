import React, { useCallback, useState, useEffect, } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableOpacity,
    Image,
    ScrollView,
    ActivityIndicator, Alert,
    Dimensions
} from 'react-native';
import axios from 'axios';
import APIKit from '../User/APIKit.js';
import { DrawerActions } from '@react-navigation/native';

import DatePicker from 'react-native-datepicker';
import DropDownPicker from 'react-native-dropdown-picker';

import { Formik, validateYupSchema } from 'formik';
import * as Yup from 'yup';

import { useSelector, useDispatch } from 'react-redux';
import * as UserActions from '../../store/Actions/User';
import { reset } from 'expo/build/AR';
import Colors from '../../constants/Colors';
const FormValidationSchema = Yup.object({
    emailAddress: Yup
        .string().required("Your email is required! ")
        .email("invalid email"),
    firstName: Yup
        .string().required("Your first name is required!"),
    lastName: Yup
        .string().required("Your last name is required!"),
    birthDay: Yup
        .string().required("Your birthday is required!"),
    mobileNumber: Yup
        .string().required("Your mobile number is required!")
        .min(9, 'The mobile number must be grater then 8 characters!')
        .max(13, 'The mobile number must be less then 14 characters!'),
    addressDetails: Yup
        .string().required("Your address details is required!"),
    regionId: Yup
        .string().required("Your region is required!"),




});
const { width: screenWidth } = Dimensions.get('window');
export const deviceWidth = Dimensions.get('window').width
export const deviceHeight = Dimensions.get('window').height
export default function SignUpScreen({ navigation }) {
    const [IsSetRegion, SetIsSetRegion] = useState(false);
    const [IsGetRegion, SetIsGetRegion] = useState(false);
    const [IsLoading, SetIsLoading] = useState(false);
    const [Errors, SetErrors] = useState([]);
    const Region = useSelector(State => State.userReducer.allRegion);
    const Categories = useSelector(State => State.categoriesReducer.allCategories);




    var Regions = [
        // { value: 1, label: 'mazzeh section' },
        // { value: 2, label: 'Ibn Asaker' },
        // { value: 3, label: 'Thawra' },
        // { value: 4, label: 'etsy' },
        // { value: 5, label: 'facebook' },
        // { value: 6, label: 'foursquare' },
        // { value: 7, label: 'github-alt' },
        // { value: 8, label: 'github' },
        // { value: 9, label: 'gitlab' },
        // { value: 10, label: 'instagram' },
    ];



    let validationError = useSelector(State => State.userReducer.userData.isErrorRegister);
    let validationErrorNumber = useSelector(State => State.userReducer.userData.isErrorRegisterNumber);
    let validationOk = useSelector(State => State.userReducer.userData.isRegister);
    const Dispatch = useDispatch();



    const LoadRegions = useCallback(async () => {
        try {
            await Dispatch(UserActions.ReginList());
            SetIsGetRegion(true);
        } catch (error) {

        } LoadRegions
        console.log(IsGetRegion);
    }, [Dispatch, SetIsGetRegion]);
    useEffect(() => {
        LoadRegions();
    }, [Dispatch, LoadRegions]);

    if (IsGetRegion) {
        Region.forEach(element => {
            Regions.push({ value: element.id, label: element.name })
        });
    }
    const Register = useCallback(async (values) => {
        SetErrors([]);
        SetIsLoading(true);
        try {
            await Dispatch(UserActions.Register(values.firstName, values.lastName, values.birthDay, values.mobileNumber, values.regionId, values.addressDetails, values.emailAddress));
            // navigation.navigate('DrawerNavigator');
            // navigation.dispatch(DrawerActions.toggleDrawer())
        } catch (error) {
            SetErrors([...Errors, error.message])
        }

        SetIsLoading(false);
    }, [Dispatch, SetIsLoading, SetErrors]);

    if (validationError) {
        //alert("user already exist ! , go to login page please");

        Alert.alert(
            'user already exist ! , go to login page please',
            '', // <- this part is optional, you can pass an empty string
            [
                { text: 'OK', onPress: () => validationError = false },
            ],
            { cancelable: false },
        );

        navigation.navigate('DrawerNavigator', { screen: 'SignInScreen' })
    }

    // if (validationErrorNumber) {
        const onFailure = (error) => {

        // alert(
        //     "invalid mobile number"
        // );

        Alert.alert(
            'user already exist ! , go to login page please',
            '', // <- this part is optional, you can pass an empty string
            [
                { text: 'OK', onPress: () => {navigation.navigate('DrawerNavigator', { screen: 'SignInScreen'})} },
            ],
            { cancelable: false },
        );

        //  navigation.navigate('DrawerNavigator', { screen: 'SignUpScreen'})
        //SetIsSetRegion(false);
        // this.IsSetRegion=false;
    }




    return (
        <View style={styles.container}>
            <Image style={styles.bgImage} source={require('../../assets/images/login.png')} />

            <Formik
                initialValues={{ firstName: '', lastName: '', birthDay: '', regionId: '', mobileNumber: '', addressDetails: '', emailAddress: '' }}
                validationSchema={FormValidationSchema}
                onSubmit={(values, actions) => {
                    // Register(values);
                    APIKit.post('http://82.165.100.137:8000/api/register?first_name='+values.firstName+'&last_name='+values.lastName+'&birthday='+values.birthDay+'$phone='+values.mobileNumber+'$region_id='+values.regionId+'$address_details='+values.addressDetails+'$email_address='+values.emailAddress )
                    .then(response => {
                        console.log("ooooookkkkkkkkkkk",response)
                        if (response.status === 200) {
                            navigation.navigate('VerificationScreen', { mobilePhoneNumber: values.mobileNumber });

                    }
                    // break;
                    }).catch(error => {
                        if(error!=null){
                            console.log("noooooooooooot",error)
                             onFailure()
                        }
                        
                    });
                    SetIsSetRegion(false);
                    actions.resetForm();
                    // if (validationOk) {
                    //     navigation.navigate('VerificationScreen', { mobilePhoneNumber: values.mobileNumber });


                    // }
                    // navigation.navigate('VerificationScreen', { mobilePhoneNumber: values.mobileNumber });
                }}
            >
                {({ handleChange, handleBlur, handleSubmit, validateForm, setTouched, values, errors, touched }) => (
                    <View style={styles.card}>
                        {IsLoading &&
                            <View style={styles.centered}>
                                <ActivityIndicator color='#fff' size='large' />
                            </View>

                        }

                        {!IsLoading &&
                            <View style={IsSetRegion ? styles.formRegionContent : styles.formContent}>
                                {!IsSetRegion ?
                                    <>
                                        <ScrollView style={{ marginBottom: 20 }} contentContainerStyle={styles.scrollViewContainer}>
                                            <View style={styles.allinputs}>
                                                <View style={styles.inputContainer}>
                                                    <TextInput style={styles.inputs}
                                                        placeholder="First Name"
                                                        keyboardType='default'
                                                        underlineColorAndroid='transparent'
                                                        onChangeText={handleChange('firstName')}
                                                        onBlur={handleBlur('firstName')}
                                                        value={values.firstName}
                                                    />

                                                    <Image style={styles.inputIcon} source={require('../../assets/images/icons/name-card.png')} />
                                                </View>

                                                {touched.firstName && errors.firstName && <Text style={styles.error}>{errors.firstName}</Text>}

                                                <View style={styles.inputContainer}>
                                                    <TextInput style={styles.inputs}
                                                        placeholder="Last Name"
                                                        keyboardType='default'
                                                        underlineColorAndroid='transparent'
                                                        onChangeText={handleChange('lastName')}
                                                        onBlur={handleBlur('lastName')}
                                                        value={values.lastName}
                                                    />

                                                    <Image style={styles.inputIcon} source={require('../../assets/images/icons/name-card.png')} />
                                                </View>

                                                {touched.lastName && errors.lastName && <Text style={styles.error}>{errors.lastName}</Text>}

                                                <View style={styles.inputContainer}>
                                                    <DatePicker
                                                        style={styles.datePickerInputs}
                                                        mode="date"
                                                        placeholder="Birthday"
                                                        format="YYYY-MM-DD"
                                                        minDate="1920-01-01"
                                                        maxDate="9999-12-30"
                                                        confirmBtnText="Confirm"
                                                        cancelBtnText="Cancel"
                                                        customStyles={{
                                                            dateIcon: {
                                                                display: 'none'
                                                            },
                                                            dateInput: {
                                                                borderWidth: 0,
                                                            }
                                                        }}
                                                        onDateChange={handleChange('birthDay')}
                                                        date={values.birthDay}
                                                    />

                                                    <Image style={styles.inputIcon} source={require('../../assets/images/icons/birthday-cake.png')} />
                                                </View>

                                                {touched.birthDay && errors.birthDay && <Text style={styles.error}>{errors.birthDay}</Text>}

                                                <View style={styles.inputContainer}>
                                                    <TextInput
                                                        style={styles.inputs}
                                                        keyboardType='numeric'
                                                        placeholder="Mobile Number"
                                                        underlineColorAndroid='transparent'
                                                        onChangeText={handleChange('mobileNumber')}
                                                        onBlur={handleBlur('mobileNumber')}
                                                        value={values.mobileNumber}
                                                    />

                                                    <Image style={styles.inputIcon} source={require('../../assets/images/icons/mobile-login.png')} />
                                                </View>

                                                {touched.mobileNumber && errors.mobileNumber && <Text style={styles.error}>{errors.mobileNumber}</Text>}

                                                <View style={[styles.inputContainer, styles.multiLineInputsContainer]}>
                                                    <TextInput style={styles.inputs}
                                                        placeholder="Your Address"
                                                        keyboardType='default'
                                                        multiline={true}
                                                        underlineColorAndroid='transparent'
                                                        onChangeText={handleChange('addressDetails')}
                                                        onBlur={handleBlur('addressDetails')}
                                                        value={values.addressDetails}
                                                    />

                                                    <Image style={styles.inputIcon} source={require('../../assets/images/icons/location.png')} />
                                                </View>

                                                {touched.addressDetails && errors.addressDetails && <Text style={styles.error}>{errors.addressDetails}</Text>}

                                                <View style={styles.inputContainer}>
                                                    <TextInput style={styles.inputs}
                                                        placeholder="Your Email Address"
                                                        keyboardType='email-address'
                                                        underlineColorAndroid='transparent'
                                                        onChangeText={handleChange('emailAddress')}
                                                        onBlur={handleBlur('emailAddress')}
                                                        value={values.emailAddress}
                                                    />

                                                    <Image style={styles.inputIcon} source={require('../../assets/images/icons/email.png')} />
                                                </View>
                                                {touched.emailAddress && errors.emailAddress && <Text style={styles.error}>{errors.emailAddress}</Text>}
                                            </View>

                                        </ScrollView>
                                        <TouchableOpacity style={[styles.buttonContainer, styles.loginButton]} onPress={() => validateForm().then(validation => validation.hasOwnProperty('emailAddress') ? setTouched(validation) : SetIsSetRegion(true))}>
                                            <Text style={styles.loginText}>Next</Text>
                                        </TouchableOpacity>

                                    </>
                                    :
                                    <>
                                        <View style={styles.reginview}>
                                            <View style={styles.dropDownPickerInputContainer}>
                                                <DropDownPicker
                                                    items={Regions}
                                                    defaultIndex={0}
                                                    style={styles.dropDownPickerInput}
                                                    containerStyle={{ height: 45 }}
                                                    onChangeItem={item => values.regionId = item.value}
                                                    searchable={true}
                                                />

                                                <View style={styles.dropDownPickerInputIcon}>
                                                    <Image style={styles.inputIcon} source={require('../../assets/images/icons/location.png')} />
                                                </View>
                                            </View>

                                            {touched.regionId && errors.regionId && <Text style={styles.dderror}>{errors.regionId}</Text>}
                                        </View>
                                        <View style={styles.signupview}>
                                            <TouchableOpacity style={[styles.buttonContainersignUp, styles.loginButton]} onPress={handleSubmit}>
                                                <Text style={styles.loginText}>Sign Up</Text>
                                            </TouchableOpacity>

                                           
                                        </View>
                                        <TouchableOpacity style={styles.buttonContainertextsignin} onPress={() => navigation.navigate('SignInScreen')}>
                                                <Text style={styles.btnText}>You're already a member? Sign In Here</Text>

                                            </TouchableOpacity>
                                    </>
                                }


                            </View>
                        }
                    </View>
                )}
            </Formik>
        </View>
    );
}

const resizeMode = 'cover';

const styles = StyleSheet.create({
    allinputs: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',

    }
    ,
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#DCDCDC',
    },
    scrollViewContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        width: screenWidth - 100,
        // flex:1,
        paddingBottom: 10,


    },
    formContent: {
        alignItems: 'center',
        flex: 1,
        marginTop: 30,
        marginRight: 30,
        justifyContent: 'space-evenly',

    },
    formRegionContent: {
        flex: 1,
        // marginTop: screenWidth - 30,
        justifyContent: 'space-between'
    },
    inputContainer: {
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        width: 200,
        height: 45,
        marginTop: 5,
        marginBottom: 5,
        flexDirection: 'row',
        alignItems: 'center',
        shadowColor: "#808080",
        shadowOpacity: 0.25,
        shadowRadius: 5,
        elevation: 6,
        // marginRight:deviceWidth-300,
    },
    dropDownPickerInputContainer: {
        borderRadius: 10,
        // marginTop:70,
        // height: 50,
        // marginBottom: 5,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: "center",
        marginTop:screenWidth-300
    },
    inputs: {
        marginLeft: 16,
        paddingRight: 10,
        borderBottomColor: '#FFFFFF',
        flex: 1,

    },
    multiLineInputsContainer: {
        height: 75,
    },
    dropDownPickerInput: {
        backgroundColor: '#fff',
        borderWidth: 0,
        borderTopRightRadius: 0,
        borderBottomRightRadius: 0,
        borderTopLeftRadius: 10,
        borderBottomLeftRadius: 10,
        width: 200,
        padding: 10,
        paddingLeft: 16,
        shadowColor: "#808080",
        shadowOpacity: 0.25,
        shadowRadius: 5,
        alignSelf: "center",
    },
    inputIcon: {
        width: 25,
        height: 25,
        marginRight: 15,
        justifyContent: 'center'
    },
    dropDownPickerInputIcon: {
        paddingVertical: 10,
        marginLeft: -3,
        paddingLeft: 10,
        paddingRight: 0,
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10,
        backgroundColor: '#fff'
    },
    datePickerInputs: {
        marginLeft: -80,
        flex: 1,
    },
    buttonContainer: {
        height: 45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 30,
        width: 200,
        borderRadius: 30,
        backgroundColor: 'transparent',
        marginLeft: 30,
        alignSelf:'center'
    },
    buttonContainersignUp: {
        height: 45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        // marginBottom: 10,
        width: screenWidth - 200,
        borderRadius: 30,
        alignSelf: 'center',
        backgroundColor: 'transparent',
        //  marginLeft: 30

    },
    buttonContainertext: {
        height: 45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 250,
        width: 200,
        borderRadius: 30,
        backgroundColor: 'transparent',
        marginLeft: 30
    },
    buttonContainertextsignin: {
        height: 45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 100,
         width:screenWidth- 100,
        borderRadius: 30,
        backgroundColor: 'transparent',
        marginTop:20,
        alignSelf:'center'
    },
    btnForgotPassword: {
        height: 15,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        marginBottom: 10,
        width: 200,
        backgroundColor: 'transparent'
    },
    loginButton: {
        backgroundColor: "#000000",
        shadowColor: "#808080",
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0,
        shadowRadius: 3,
        elevation: 15,

    },
    loginText: {
        color: 'white',
    },
    bgImage: {
        flex: 1,
        resizeMode,
        position: 'absolute',
        width: '100%',
        height: '100%',
        justifyContent: 'center',
    },
    btnText: {
        color: "white",
        fontWeight: 'bold'
    },
    error: {
        // marginBottom: 15,
        color: "white",
        fontWeight: 'bold',
        width: 200,
        justifyContent: 'center',
        alignItems: 'center',
        // marginLeft:40
    },
    dderror: {
        color: "white",
        fontWeight: 'bold',
        // alignSelf:"center",
        marginTop: 20,
        // flexDirection: 'row',
        alignItems: 'center',
        justifyContent: "center",
        alignSelf: "center"
    },
    centered: {
        flex: 1,
        marginTop: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    card: {
        backgroundColor: Colors.activeSelected,
        width: deviceWidth - 100,
        // flex: 1,
        height: deviceHeight-200,
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,
        elevation: 24,
        shadowColor: "#000000",
        // alignItems: 'center',
        // justifyContent: 'center',
        borderRadius: 10,
        margin: 10

    },
    reginview: {
        width: screenWidth - 100,
        padding: 10,
        // height: screenWidth - 500,
        alignItems:'center',
    },
    signupview:{
        width: screenWidth - 100,
        padding: 10,
        // paddingTop: 50,
        // height: screenWidth - 500,

    }

});