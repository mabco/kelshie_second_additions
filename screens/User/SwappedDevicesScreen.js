import * as React from 'react';
import { StyleSheet, Text } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { CustomText } from '../../components/UI/StyledText';

export default function SwappedDevicesScreen() {
    return (
        <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
            <CustomText style={styles.comingSoon} isArabic={true}>قريبا ...</CustomText>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fafafa',
    },
    contentContainer: {
        paddingTop: 15,
        paddingLeft: 15
    },
    comingSoon: {
        fontSize: 40,
        textAlign: 'center',
        marginTop: 30,
    }
});