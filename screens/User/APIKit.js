import axios from 'axios';
import GetAPI_BaseURL from '../../constants/API/Connection';

const API_BaseURL = GetAPI_BaseURL();

// Create axios client, pre-configured with baseURL
let APIKit = axios.create({
  baseURL: API_BaseURL,
  timeout: 10000,
});

export default APIKit;
