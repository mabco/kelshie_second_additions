import React, { useState, useEffect, useCallback } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableOpacity,
    Image,
    Alert,Dimensions
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { Formik } from 'formik';
import * as Yup from 'yup';
import * as UserActions from '../../store/Actions/User';
import axios from 'axios';
import APIKit from '../User/APIKit.js';
import Colors from '../../constants/Colors';
const FormValidationSchema = Yup.object({
    mobileNumber: Yup
        .string().required("Your mobile number is required!")
        .min(9, 'The mobile number must be grater then 8 characters!').max(13, 'The mobile number must be less then 14 characters!')
});

const { width: screenWidth } = Dimensions.get('window');

export default function SignInScreen({ navigation }) {
    const Dispatch = useDispatch();
    let validationError = useSelector(State => State.userReducer.userData.isVcodeErroe);
    const [IsLoading, SetIsLoading] = useState(false);
    const [Errors, SetErrors] = useState([]);
    const isLogged = useSelector(State => State.userReducer.userData.IsVcodeOK);

    // const SendCode = useCallback(async (MobilePhoneNumber) => {
    //     SetErrors([]);
    //     SetIsLoading(true);
    //     try {
    //         // console.log("boooddyyyyy", MobilePhoneNumber);
    //         await Dispatch(UserActions.SendVerificationCode(MobilePhoneNumber));

    //     } catch (error) {
    //         SetErrors([...Errors, error.message])

    //     }
    //     //SetIsLoading(false);
    // }, [Dispatch, SetIsLoading, SetErrors])

    // useEffect(() => {
    //     SendCode();
    // }, [Dispatch, SendCode]);


    
    // if (validationError) {

    //     Alert.alert(
    //         'User not found Pleas Register First',
    //         '', // <- this part is optional, you can pass an empty string
    //         [
    //             {
    //                 text: 'OK', onPress: () => {

    //                     navigation.navigate('DrawerNavigator', { screen: 'SignUpScreen' });

    //                 }
    //             },
    //         ],
    //         // { cancelable: false },
    //     );
    //     //SetvalidationError(false);
    //     // validationError=false
    //     // alert("User not found Pleas Register First");



    // }
  
    // const onSuccess = ({data}) => {
    //     navigation.navigate('VerificationScreen', { mobilePhoneNumber: data });
    //   };
      const onFailure = error => {
        console.log(error && error.response);
            Alert.alert(
            'User not found Pleas Register First',
            '', // <- this part is optional, you can pass an empty string
            [
                {
                    text: 'OK', onPress: () => navigation.navigate('DrawerNavigator', { screen: 'SignUpScreen' })
                },
            ],
            // { cancelable: false },
        );
      };
    return (
        <View style={styles.container}>
            <Image style={styles.bgImage} source={require('../../assets/images/login.png')} />

            <Formik
                initialValues={{ mobileNumber: '' }}
                validationSchema={FormValidationSchema}
                onSubmit={(values, actions) => {

                    APIKit.post('http://82.165.100.137:8000/api/sendVerificationCode?phone='+values.mobileNumber )
                    .then(response => {
                        if (response.status === 200) {
                            //proceed...
                            navigation.navigate('VerificationScreen', { mobilePhoneNumber: values.mobileNumber })

                        }
                    //     else {
                    //         // throw error and go to catch block
                    //         onFailure();
                    //         // throw new Error("Error");
                    // }
                }).catch(error => {
                    
                    onFailure()});
                
                    // SendCode(values.mobileNumber);
                    // console.log("submit body");

                    // Dispatch(UserActions.SendVerificationCode(values.mobileNumber));
                    // SetIsError(validationError);
                    // if (isLogged) { navigation.navigate('VerificationScreen', { mobilePhoneNumber: values.mobileNumber }); }

                    actions.resetForm();
                }}
            >
                {({ handleChange, handleBlur, handleSubmit, values, errors, touched }) => (
                    <View style={styles.card}>
                        <View style={styles.inputContainer}>
                            
                            <TextInput
                                style={styles.inputs}
                                keyboardType='numeric'
                                placeholder="Mobile Number"
                                underlineColorAndroid='transparent'
                                onChangeText={handleChange('mobileNumber')}
                                onBlur={handleBlur('mobileNumber')}
                                value={values.mobileNumber}
                            />

                            <Image style={styles.inputIcon} source={require('../../assets/images/icons/mobile-login.png')} />
                        </View>

                        {touched.mobileNumber && errors.mobileNumber && <Text style={styles.error}>{errors.mobileNumber}</Text>}

                        <TouchableOpacity style={[styles.buttonContainer, styles.loginButton]} onPress={handleSubmit}>
                            <Text style={styles.loginText}>Sign In</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={styles.buttonContainertext} onPress={() => navigation.navigate('SignUpScreen')}>
                <Text style={styles.btnText}>Not a member? Sign Up Here</Text>
            </TouchableOpacity>
                    </View>
                )}
            </Formik>

           
        </View>
    );
}

const resizeMode = 'cover';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#DCDCDC',
    },
    inputContainer: {
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        width: screenWidth-150,
        height: screenWidth-500,
        marginTop: 10,
        flexDirection: 'row',
        alignItems: 'center',
        shadowColor: "#808080",
        shadowRadius: 5,
        elevation: 6,
        
        
    },
    inputs: {
        height: 45,
        marginLeft: 16,
        paddingRight: 10,
        borderBottomColor: '#FFFFFF',
        flex: 1,
    },
    inputIcon: {
        width: 25,
        height: 25,
        marginRight: 15,
        justifyContent: 'center'
    },
    buttonContainer: {
        height: 45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20,
        width: screenWidth-150,
        borderRadius: 30,
        backgroundColor: 'transparent'
    },
    buttonContainertext: {
        height: 45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        
        marginTop: 10,
        width: 300,
        borderRadius: 30,
        backgroundColor: 'transparent'
    },
    loginButton: {
        backgroundColor: "#000000",
        shadowColor: "#808080",
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0,
        shadowRadius: 3,
        elevation: 15,
        
    },
    loginText: {
        color: 'white',
    },
    bgImage: {
        flex: 1,
        resizeMode,
        position: 'absolute',
        width: '100%',
        height: '100%',
        justifyContent: 'center',
    },
    btnText: {
        color: "white",
        fontWeight: 'bold'
    },
    error: {
        marginTop: 10,
        color: 'white',
        fontWeight: 'bold',
        alignSelf:"auto",
        // alignItems:"stretch",
        
    },
    card: {
        backgroundColor: Colors.activeSelected,
         width: screenWidth-100,
        height: screenWidth-100,
        // flex: 1,
        shadowOffset: {
            width: 0,   
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,
        elevation: 24,
        shadowColor: "#000000",
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius:10
        
        
      }
});