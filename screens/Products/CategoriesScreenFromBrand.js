import React, { useState, useEffect, useCallback } from 'react';
import { StyleSheet,  View, FlatList, ActivityIndicator, Button } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import * as CategoriesActions from '../../store/Actions/Categories';
import CategoryItem from '../../components/Products/CategoryItem';
import  {CustomText }  from '../../components/UI/StyledText';
import * as ProductActions from '../../store/Actions/Products';
export default function CategoriesScreenFromBrand({ navigation ,route}) {
    const [IsLoading, SetIsLoading] = useState(false);
    const [Errors, SetErrors] = useState([]);
    const Categories = useSelector(State => State.categoriesReducer.allCategories);
    const BrandID = route.params.BrandID;
    console.log(BrandID)
    const Dispatch = useDispatch();
    navigation.setOptions({ title: route.params.title+' Categories' })
    const LoadCategories = useCallback(async () => {
        SetErrors([]);
        SetIsLoading(true);

        try {
            await Dispatch(CategoriesActions.GetCategories());
        } catch (error) {
            SetErrors([...Errors, error.message])
        }

        SetIsLoading(false);
    }, [Dispatch, SetIsLoading, SetErrors]);

    useEffect(() => {
        LoadCategories();
    }, [Dispatch, LoadCategories]);


    const LoadProduct = useCallback(async (value) => {
        try {
            await Dispatch(ProductActions.GetProductByCatBrand(value,BrandID));
        } catch (error) {  
        }
    }, [Dispatch]);

    useEffect(() => {
        LoadProduct();
    }, [Dispatch, LoadProduct]);

    // useEffect(() => {
    //     navigation.addListener('focus', LoadCategories);
    // }, [LoadCategories]);

    if (Errors.length > 0)
        return <View style={styles.centered}>
          <CustomText isArabic={false}>Error occurred!</CustomText>
            <Button title='Try Again' onPress={LoadCategories} />
        </View>

    if (IsLoading)
        return <View style={styles.centered}>
            <ActivityIndicator size='large' />
        </View>

    if (!IsLoading && Categories.length === 0)
        return <View style={styles.centered}>
            <CustomText isArabic={false}>No categories found!! Come back soon.</CustomText>
            <Button title='Try Again' onPress={LoadCategories} />
        </View>

    return (
        <FlatList style={styles.list}
            data={Categories}
            keyExtractor={(Item) => {
                return Item.id.toString();
            }}
            ItemSeparatorComponent={() => {
                return (
                    <View style={styles.separator} />
                )
            }}
            renderItem={(post) => {
                const Item = post.item;
                
                return (
                    <CategoryItem
                        name={Item.name}
                        image={Item.image}
                        onViewCategory={() => {
                            LoadProduct(Item.id)
                            navigation.navigate('ProductsScreenBrand', { SelectedCategoryID: Item.id , BrandID: BrandID });
                        }}
                    />
                )
            }}
        />
    );
}

const styles = StyleSheet.create({
    list: {
        backgroundColor: "#E6E6E6",
    },
    separator: {
        marginTop: 1,
    },
    centered: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});