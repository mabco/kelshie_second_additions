import React, { useState, useEffect, useCallback } from 'react';
import { StyleSheet, View, Image, TouchableOpacity, Dimensions, Platform, FlatList,BackHandler } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import Carousel, { ParallaxImage, Pagination } from 'react-native-snap-carousel';
import { useSelector, useDispatch } from 'react-redux';
import * as CartActions from '../../store/Actions/Cart';

import * as ProductActions from '../../store/Actions/Products';
import Colors from '../../constants/Colors';
import { CustomText } from '../../components/UI/StyledText';

// Navigation imports
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import CustomHeaderButton from '../../components/UI/HeaderButton';

const { width, height } = Dimensions.get('window');
const SCREEN_WIDTH = width < height ? width : height;
const ProductNumColumns = 2;
const Product_ITEM_HEIGHT = 150;
const Product_ITEM_MARGIN = 10;
const { width: screenWidth } = Dimensions.get('window');

export default function ProductOverviewScreen({ route, navigation }) {
    const SelectedShopID = route.params.selectedShopID;
    const SelectedProductID = route.params.selectedProductID;
    const SelectedProduct = useSelector(State =>
        State.shopsReducer.allShops[SelectedShopID]
            .products.find(Product => Product.id === SelectedProductID)
    );



    const [IsSignedIn, SetIsSignedIn] = React.useState(true);
    const [IsSpec, SetIsSpec] = useState(false);
    // const Orders = useSelector(State => State.ordersReducer.orders);
    const Spec = useSelector(State => State.productsReducer.ProductSpecs);
    const [IsLoading, SetIsLoading] = useState(false);
    const isLogged = useSelector(State => State.userReducer.userData.isLoggedIn);
    const [Errors, SetErrors] = useState([]);
    const [Price, SetPrice] = React.useState(0);
    // console.log("SelectedProduct", Orders);
    navigation.setOptions({
        headerTitle: 'Products: ' + SelectedProduct.name,
        headerRight: () => (
            <HeaderButtons HeaderButtonComponent={CustomHeaderButton}>
                <Item title='Menu' iconName={Platform.OS === 'android' ? "md-cart" : "ios-cart"} onPress={() => CartActions.AddToCart(SelectedProduct)} />
            </HeaderButtons>
        ),
    });
    const Dispatch = useDispatch();

    const [entries, setEntries] = useState([]);
    useEffect(() => { setEntries(oneproduct); }, []);

    const oneproduct = [
        // {
        //     illustration: 'https://i.imgur.com/UPrs1EWl.jpg',
        // },
        // {
        //     illustration: 'https://i.imgur.com/UPrs1EWl.jpg',
        // },
        // {
        //     illustration: 'https://i.imgur.com/MABUbpDl.jpg',
        // },
        // {
        //     illustration: 'https://i.imgur.com/KZsmUi2l.jpg',
        // },
        // {
        //     illustration: 'https://i.imgur.com/2nCt3Sbl.jpg',
        // },
    ];


//Handler BackPress
//, onPress: () => validationError = false
function handleBackButtonClick() {
    navigation.navigate('ShopOverviewScreen')
    return true;
  }

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
    };
  }, []);


    let [CurrentSlider, SetCurrentSlider] = useState(0);



    SelectedProduct.images.forEach(element => {

        oneproduct.push({ illustration: element.image_name })
    });



    const LoadSpecs = useCallback(async () => {
        SetErrors([]);
        SetIsLoading(true);
        try {
            await Dispatch(ProductActions.GetProductDetails(SelectedProduct.id));
        } catch (error) {
            SetErrors([...Errors, error.message])
        }
        SetIsLoading(false);
        SetIsSpec(true);
    }, [Dispatch, SetIsLoading, SetErrors]);
    useEffect(() => {
        LoadSpecs();
    }, [LoadSpecs, SetIsLoading]);

    console.log(IsSpec);

    // if (typeof Spec != 'undefined') {
    //     SetIsSpec(true)
    // }
    function CheckLogin(Item) {
        if (isLogged) {
           
            Dispatch(CartActions.AddToCart(Item))


        } else {
            navigation.navigate('DrawerNavigator', { screen: 'SignInScreen' })

        }
    }




    const renderItem = ({ item, index }, parallaxProps) => {
        return (

            <View style={styles.item}>
                <ParallaxImage
                    source={{ uri: item.illustration }}
                    containerStyle={styles.imageContainer}
                    style={styles.image}
                    parallaxFactor={0.5}
                    {...parallaxProps}
                />
            </View>

        );
    };



    return (
        <ScrollView>
            <View style={styles.container}>

                <Carousel
                    sliderWidth={screenWidth}
                    sliderHeight={screenWidth}
                    itemWidth={screenWidth - 60}
                    data={entries}
                    renderItem={renderItem}
                    hasParallaxImages={true}
                    onSnapToItem={index => SetCurrentSlider(index)}
                    autoplay={true}
                    isLooped={true}
                    loop={true}
                />
                <Pagination
                    dotsLength={entries.length}
                    containerStyle={styles.paginationContainer}
                    activeDotIndex={CurrentSlider}
                    dotColor="rgba(255, 255, 255, 0.92)"
                    dotStyle={styles.paginationDot}
                    inactiveDotColor="white"
                    inactiveDotOpacity={0.4}
                    inactiveDotScale={0.6}
                />
                <View style={styles.line}></View>
                <View style={styles.orcontainer}>
                    <CustomText isArabic={false} style={styles.name}>{SelectedProduct.name}</CustomText>
                    <CustomText isArabic={false} style={styles.Brandname}> - </CustomText>
                    <TouchableOpacity><CustomText isArabic={false} style={styles.Brandname}>Samsung</CustomText></TouchableOpacity>
                </View>

                <CustomText isArabic={false} style={styles.price}>Price: {SelectedProduct.shop_products[0].price} SP</CustomText>


                <TouchableOpacity><CustomText isArabic={false} style={styles.colorname}>Available colors</CustomText></TouchableOpacity>
                <View style={styles.contentColors}>

                    {SelectedProduct.images.map(function (item) {
                        return (
                            <TouchableOpacity style={[styles.btnColor, { backgroundColor: item.color_hexa }]}></TouchableOpacity>

                        );
                    }.bind(this))}



                    {/* {SelectedProduct.images.forEach(element => {
                        
                        return(
                         <TouchableOpacity style={[styles.btnColor, { backgroundColor: element.color_hexa }]}></TouchableOpacity>
                        )
                        })
                        } */}
                    {/* <TouchableOpacity style={[styles.btnColor, { backgroundColor: "#000" }]}></TouchableOpacity>
                    <TouchableOpacity style={[styles.btnColor, { backgroundColor: "#515A5A" }]}></TouchableOpacity>
                    <TouchableOpacity style={[styles.btnColor, { backgroundColor: "#fff" }]}></TouchableOpacity>
                    <TouchableOpacity style={[styles.btnColor, { backgroundColor: "#922B21" }]}></TouchableOpacity>
                    <TouchableOpacity style={[styles.btnColor, { backgroundColor: "#D4AC0D" }]}></TouchableOpacity>
                    <TouchableOpacity style={[styles.btnColor, { backgroundColor: "#17A589" }]}></TouchableOpacity> */}

                </View>


                {!IsSpec &&
                    <React.Fragment>
                        <CustomText isArabic={false} style={styles.specificationtext}>SPECIFICATION</CustomText>
                        <View style={styles.specificationBox}>
                            <View style={styles.specimagecontainer}>
                                <Image style={styles.listimage} source={require('../../assets/images/display.png')} />
                            </View>

                            <CustomText isArabic={false} style={styles.specification}>Quad-core (2x2.15 GHz Kryo) Quad-core (2x2.15 GHz Kryo)</CustomText>
                        </View>
                        <View style={styles.specificationBox}>
                            <Image style={styles.listimage} source={{ uri: 'https://image.flaticon.com/icons/png/512/2165/2165105.png' }} />
                            <CustomText isArabic={false} style={styles.specification}>Quad-core (2x2.15 GHz Kryo) Quad-core (2x2.15 GHz Kryo)</CustomText>
                        </View>
                        <View style={styles.specificationBox}>
                            <Image style={styles.listimage} source={{ uri: 'https://image.flaticon.com/icons/png/512/883/883787.png' }} />
                            <CustomText isArabic={false} style={styles.specification}>Quad-core (2x2.15 GHz Kryo) Quad-core (2x2.15 GHz Kryo)</CustomText>
                        </View>
                        <View style={styles.specificationBox}>
                            <Image style={styles.listimage} source={require('../../assets/images/sim-card.png')} />
                            <CustomText isArabic={false} style={styles.specification}>Quad-core (2x2.15 GHz Kryo) Quad-core (2x2.15 GHz Kryo)</CustomText>
                        </View>
                        <View style={styles.specificationBox}>
                            <Image style={styles.listimage} source={{ uri: 'https://image.flaticon.com/icons/png/512/978/978073.png' }} />
                            <CustomText isArabic={false} style={styles.specification}>Quad-core (2x2.15 GHz Kryo) Quad-core (2x2.15 GHz Kryo) </CustomText>
                        </View>
                        <View style={styles.line}></View>
                        <View style={styles.addToCarContainer}>
                            <TouchableOpacity style={styles.shareButton} onPress={() => CheckLogin(SelectedProduct)}>
                                <CustomText isArabic={false} style={styles.shareButtonText}>Add To Cart</CustomText>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.line}></View>
                    </React.Fragment>
                }



                {IsSpec &&
                    <React.Fragment>
                        <CustomText isArabic={false} style={styles.specificationtext}>SPECIFICATION</CustomText>
                        <View style={styles.specificationBox}>
                            <View style={styles.specimagecontainer}>
                                <Image style={styles.listimage} source={require('../../assets/images/display.png')} />
                            </View>

                            <CustomText isArabic={false} style={styles.specification}>{Spec.specs[1].spec_title} {Spec.specs[1].product_specs.value} inch</CustomText>
                        </View>
                        <View style={styles.specificationBox}>
                            <Image style={styles.listimage} source={{ uri: 'https://image.flaticon.com/icons/png/512/2165/2165105.png' }} />
                            <CustomText isArabic={false} style={styles.specification}>{Spec.specs[0].spec_title} {Spec.specs[0].product_specs.value}GB, {Spec.specs[2].spec_title} {Spec.specs[2].product_specs.value}GB </CustomText>
                        </View>
                        <View style={styles.specificationBox}>
                            <Image style={styles.listimage} source={{ uri: 'https://image.flaticon.com/icons/png/512/883/883787.png' }} />
                            <CustomText isArabic={false} style={styles.specification}>{Spec.specs[5].spec_title} {Spec.specs[5].product_specs.value} pixels(wide)</CustomText>
                        </View>
                        <View style={styles.specificationBox}>
                            <Image style={styles.listimage} source={require('../../assets/images/sim-card.png')} />
                            <CustomText isArabic={false} style={styles.specification}>{Spec.specs[4].spec_title} {Spec.specs[4].product_specs.value} </CustomText>
                        </View>
                        <View style={styles.specificationBox}>
                            <Image style={styles.listimage} source={{ uri: 'https://image.flaticon.com/icons/png/512/978/978073.png' }} />
                            <CustomText isArabic={false} style={styles.specification}>{Spec.specs[3].spec_title} {Spec.specs[3].product_specs.value} mAh </CustomText>
                        </View>
                        <View style={styles.line}></View>
                        <View style={styles.addToCarContainer}>
                            <TouchableOpacity style={styles.shareButton} onPress={() => CheckLogin(SelectedProduct)}>
                                <CustomText isArabic={false} style={styles.shareButtonText}>Add To Cart</CustomText>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.line}></View>
                    </React.Fragment>
                }
            </View>
        </ScrollView >

    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 5,
    },
    orcontainer: {
        flexWrap: 'wrap',
        alignItems: 'flex-start',
        flexDirection: 'row',
    },
    line: {
        height: 2,
        backgroundColor: '#5F717C',
        width: screenWidth,
        marginTop: 20,
        marginBottom: 10
    },
    shareButtonText: {
        color: "#FFFFFF",
        fontSize: 20,
    },
    addToCarContainer: {
        marginHorizontal: 30
    },
    paginationDot: {
        width: 8,
        height: 8,
        borderRadius: 4,
        marginHorizontal: 0
    },
    paginationContainer: {
        flex: 1,
        position: 'absolute',
        alignSelf: 'center',
        paddingVertical: 8,
        marginTop: 200
    },
    item: {
        width: screenWidth - 60,
        height: screenWidth - 25,
    },
    paginationContainer: {
        flex: 1,
        position: 'absolute',
        alignSelf: 'center',
        paddingVertical: 8,
        marginTop: screenWidth - 50,
    },
    imageContainer: {
        flex: 1,
        
        marginBottom: Platform.select({ ios: 0, android: 1 }),
        backgroundColor: 'white',
        borderRadius: 8,

    },
    image: {
         ...StyleSheet.absoluteFillObject,
        resizeMode: 'contain',
    },
    productImg: {
        width: 200,
        height: 200,
    },
    name: {
        marginBottom: 0,
        marginLeft: 20,
        fontSize: 28,
        alignItems: 'center',
        color: "#1C8ECD",

        textAlign: 'center'
    },
    Brandname: {
        marginTop: 5,
        fontSize: 24,

        color: "#014B77",

        textAlign: 'left'
    },
    colorname: {
        marginTop: 15,
        fontSize: 24,
        marginHorizontal: 30,
        color: "#014B77",
        fontWeight: 'bold',
        textAlign: 'center',
    },
    Fullname: {
        marginTop: 5,
        fontSize: 20,
        marginHorizontal: 30,
        color: "#4E5E68",
        fontWeight: 'bold',
        textAlign: 'left'
    },
    price: {
        marginTop: 10,
        marginBottom: 10,
        fontSize: 25,
        color: "#6FBD3F",
        fontWeight: 'bold',
        textAlign: 'center'
    },
    description: {
        textAlign: 'center',
        marginTop: 10,
        color: "#696969",
    },
    star: {
        width: 40,
        height: 40,
    },
    btnColor: {
        height: 40,
        width: 40,
        marginHorizontal: 3,
        shadowColor: '#000',
        shadowRadius: 2,
        borderRadius: 5
    },
    btnSize: {
        height: 40,
        width: 40,
        borderRadius: 40,
        borderColor: '#778899',
        borderWidth: 1,
        marginHorizontal: 3,
        backgroundColor: 'white',

        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    starContainer: {
        justifyContent: 'center',
        marginHorizontal: 30,
        flexDirection: 'row',
        marginTop: 20
    },
    contentColors: {
        justifyContent: 'center',
        marginHorizontal: 30,
        flexDirection: 'row',
        marginTop: 10
    },
    contentSize: {
        justifyContent: 'center',
        marginHorizontal: 30,
        flexDirection: 'row',
        marginTop: 20
    },
    separator: {
        height: 2,
        backgroundColor: "#eeeeee",
        marginTop: 20,
        marginHorizontal: 30
    },
    shareButton: {
        marginTop: 10,
        height: 45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        
        backgroundColor: Colors.activeSelected,
    },
    shareButtonText: {
        color: "#000000",
        fontSize: 20,
    },
    addToCarContainer: {
        marginHorizontal: 30
    },
    specificationBox: {
        paddingTop: 10,
        paddingBottom: 10,
        marginTop: 5,
        marginRight: 5,
        marginLeft: 5,
        backgroundColor: '#E8E8E8',
        flexDirection: 'row',
        borderRadius: 10,
    },
    specificationtext: {
        fontSize: 22,
        fontWeight: 'bold',
        color: "#014B77",
        marginLeft: 10,
        alignSelf: 'center',
        marginBottom: 10,
    },
    specification: {
        fontSize: 18,
        fontWeight: 'bold',
        color: "#1E375B",
        marginLeft: 10,
        marginRight: 50,
        alignSelf: 'center',
    },
    listimage: {
        width: 35,
        height: 35,
        alignSelf: 'center',
        marginLeft: 5,

    },
    specimagecontainer: {
        width: 45,
        height: 45,
        display: 'flex',
        justifyContent: 'center',
        overflow: 'hidden'
    },
});

