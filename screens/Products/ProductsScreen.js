import React, { useState, useEffect, useCallback } from 'react';
import { StyleSheet, View, Text, Button, Dimensions, TouchableOpacity, Alert, FlatList, Platform,ActivityIndicator } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import * as CartActions from '../../store/Actions/Cart';
import ProductItem from '../../components/Products/ProductItem';
import { CustomText } from '../../components/UI/StyledText';
import * as ProductActions from '../../store/Actions/Products';

// Navigation imports
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import CustomHeaderButton from '../../components/UI/HeaderButton';
const { width: screenWidth } = Dimensions.get('window');
// const PRODUCT = [
//     { id: 1, name: "12345678", brand: "1234567890", price: 400.50, image: "http://74.208.125.168:8080/images/Samsung/SMA207FZDVMID%20s.jpg" },
//     { id: 2, name: "A91", brand: "infinix", price: 800.000, image: "http://74.208.125.168:8080/images/Samsung/SMA307FZGWMID%20S.jpg" },
//     { id: 3, name: "A52", brand: "samsung", price: 600.000, image: "http://74.208.125.168:8080/images/Samsung/SMA515FZIHMID%20S.jpg" },
//     { id: 4, name: "A71", brand: "samsung", price: 500.000, image: "http://74.208.125.168:8080/images/HUAWEI/6901443369331%20S.jpg" },
//     { id: 5, name: "A80", brand: "samsung", price: 1300.000, image: "http://74.208.125.168:8080/images/HUAWEI/6901443319602%20S.JPG" },
//     { id: 6, name: "A90", brand: "samsung", price: 1000.000, image: "http://74.208.125.168:8080/images/Samsung/SMN975FZWDMID%20S.jpg" },
//     { id: 7, name: "HTC U", brand: "htc", price: 900.000, image: "http://74.208.125.168:8080/images/Samsung/SMG975FZWGMID%20S.jpg" },
//     { id: 8, name: "SONY 8", brand: "sony", price: 800.000, image: "http://74.208.125.168:8080/images/Samsung/SMA715FZVDMID%20S.jpg" },
//     { id: 9, name: "XAIOMI 9", brand: "xiaomi", price: 500.000, image: "http://74.208.125.168:8080/images/Samsung/SMA015FZRDMID%20S.jpg" },
//     { id: 9, name: "HUAWI 10", brand: "huawi", price: 100.000, image: "http://74.208.125.168:8080/images/Xiaomi/6941059626343%20s.jpg" },
// ]

export default function ProductsScreen({ route, navigation }) {
    const SelectedCategoryID = route.params.SelectedCategoryID;
    
    const Product = useSelector(State => State.productsReducer.ProductByCat);
    const [IsGetRegion, SetIsGetRegion] = useState(false);
    const [IsLoading, SetIsLoading] = useState(false);
    const [IsSort, SetIsSort] = useState(false);
    const [Errors, SetErrors] = useState([]);

    const Dispatch = useDispatch();

    const SelectedCategory = useSelector(State =>
        State.categoriesReducer.allCategories.find(Category => Category.id === SelectedCategoryID)
    );

    navigation.setOptions({
        headerTitle: SelectedCategory.name + ' Category',
        headerRight: () => (
            <HeaderButtons HeaderButtonComponent={CustomHeaderButton}>
                <Item title='Menu' iconName={Platform.OS === 'android' ? "md-cart" : "ios-cart"} onPress={() => navigation.navigate('CartScreen')} />
            </HeaderButtons>
        ),
    });

    let ProductList = useSelector(State => {
        const MovingProductList = [];
        let image=""
        let shopLogo=""
        let price=""
        let shop_products=[]

        for (const id in State.productsReducer.ProductByCat.data) {
            //console.log("kkkkkkkkkkkk",id)
            // if (State.productsReducer.ProductByCat.data.hasOwnProperty(id)) {
                // console.log("lllllllllllllllll",State.productsReducer.ProductByCat.data)
                if (State.productsReducer.ProductByCat.data[id].images.length !=0) {
                    image= State.productsReducer.ProductByCat.data[id].images[0].image_name

                }
                if (State.productsReducer.ProductByCat.data[id].shop_products.length !=0) {
                    shopLogo= State.productsReducer.ProductByCat.data[id].shop_products[0].shop.logo_image
                    price= State.productsReducer.ProductByCat.data[id].shop_products[0].price
                    shop_products=State.productsReducer.ProductByCat.data[id].shop_products
                }


                MovingProductList.push({
                    id: id,
                    name: State.productsReducer.ProductByCat.data[id].name,
                    brand: State.productsReducer.ProductByCat.data[id].brand.name,
                    price: price,
                    image:image,
                    shopLogo: shopLogo,
                    shop_products:shop_products,
                    navigation:navigation
                });
            // }
        }

        return MovingProductList.sort((S1, S2) => S1.id > S2.id ? 1 : -1);
    });


    const LoadProduct = useCallback(async () => {
        SetIsLoading(true);

        try {
            await Dispatch(ProductActions.GetProductByCat(SelectedCategoryID));
        } catch (error) {
            SetErrors([...Errors, error.message])
        }

        SetIsLoading(false);
    }, [Dispatch, SetIsLoading, SetErrors]);

    useEffect(() => {
        SetErrors([]);
        LoadProduct();
    }, [SetErrors, LoadProduct]);

    // const Products = useSelector(State => State.productsReducer.availableProducts); Get all products in this cat only
    if (Errors.length > 0)
        return <View style={styles.centered}>
            <CustomText isArabic={true}>Error occurred!</CustomText>
            <Button title='Try Again' onPress={LoadProduct()} />
        </View>

    if (IsLoading)
        return <View style={styles.centered}>
            <ActivityIndicator size='large' />
        </View>

    if (!IsLoading && ProductList.length === 0)
        return <View style={styles.centered}>
            <CustomText isArabic={true}>No Product found!! Come back soon.</CustomText>
            <Button title='Try Again' onPress={LoadProduct} />
        </View>

    return (
        <View style={styles.container}>
            <View style={styles.Header}>
                <View style={styles.socialBarContainer}>
                    <View style={styles.socialBarSection}>
                        <TouchableOpacity style={styles.socialBarButton} onPress={() => { alert("New Products") }}>
                            <Text style={styles.oldnewtext}>New</Text>
                        </TouchableOpacity>
                    </View>
                    {/* <View style={styles.socialBarSection}>
                        <TouchableOpacity style={styles.socialBarButton} onPress={() => { alert("Used Products") }}>
                            <Text style={styles.oldnewtext}>Uesd</Text>
                        </TouchableOpacity>
                    </View> */}
                </View>
            </View>

            <FlatList
                style={styles.list}
                contentContainerStyle={styles.listContainer}
                data={ProductList}
                horizontal={false}
                numColumns={2}
                keyExtractor={(item) => {
                    return item.id;
                }}
                ItemSeparatorComponent={() => {
                    return (
                        <View style={styles.separator} />
                    )
                }}
                renderItem={(post) => {
                    const item = post.item;
                    return (
                        <ProductItem
                            name={item.name}
                            showShopLogo={true}
                            // shopId={2}
                            brand={item.brand}
                            brandIcon={item.brand.icon}
                            image={item.image}
                            price={item.price}
                            shopLogo={item.shopLogo}
                            shop_products={item.shop_products}
                            navigation={item.navigation}
                            onPress={() => navigation.navigate('ProductOverviewScreen', { SelectedProductID: Item.id })}
                            onViewBrandPress={() => { alert("View Brand Products") }}
                            onAddToCartPress={() => Dispatch(CartActions.AddToCart(item))}
                        />
                    )
                }} />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    list: {
        backgroundColor: "#E6E6E6",
        paddingVertical: 12
    },
    listContainer: {
        alignItems: 'center',
        marginRight: 3,
        paddingBottom: 25
    },
    separator: {
        marginTop: 3,
    },
    oldnewtext: {
        fontSize: 20,
        color: "#0B5D8A",
    },
    Header: {
        shadowOpacity: 0.5,
        shadowRadius: 4,
        marginVertical: 8,
        flexBasis: '5%',
        marginHorizontal: 5,
        borderRadius: 10,
    },
    /******** social bar ******************/
    socialBarContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        flex: 1
    },
    socialBarSection: {
        justifyContent: 'center',
        flexDirection: 'row',
        flex: 1,
    },
    socialBarlabel: {
        marginLeft: 8,
        alignSelf: 'flex-end',
        justifyContent: 'center',
    },
    socialBarButton: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    socialBarButton2: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    Selectedtab1: {
        height: 3,
        width: (screenWidth / 2) - 10,
        backgroundColor: '#FCA311',
        position: 'absolute',
        opacity: 1,
        bottom: -8,
    },
    Selectedtab2: {
        height: 3,
        width: (screenWidth / 2) - 10,
        backgroundColor: '#FCA311',
        position: 'absolute',
        opacity: 0,
        bottom: -8,
        left: screenWidth - ((screenWidth / 2) - 10)
    },
});   