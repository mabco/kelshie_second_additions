import React, {useState,useEffect,useCallback} from 'react';
import {StyleSheet,View,FlatList,ActivityIndicator,Button} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import * as ShopsActions from '../../store/Actions/Shops';
import ShopItem from '../../components/Shop/ShopItem';
import {CustomText } from '../../components/UI/StyledText';


export default function ShopsScreenProduct({ navigation, route }) {
    const [IsLoading, SetIsLoading] = useState(false);
    const [Errors, SetErrors] = useState([]);
    const ShopsScreenProduct = route.params.ShopsScreenProduct;
    let ShopsList = useSelector(State => {
        const MovingShopsList = [];
        for (const Key in ShopsScreenProduct) {
            // if (State.shopsReducer.allShops.hasOwnProperty(Key)) {
                MovingShopsList.push({
                    id: Key,
                    name: ShopsScreenProduct[Key].shop.name,
                    logo_image: ShopsScreenProduct[Key].shop.logo_image,
                    cover_image: ShopsScreenProduct[Key].shop.cover_image,
                    closed_time: ShopsScreenProduct[Key].shop.closed_time,
                    avg_rating: ShopsScreenProduct[Key].shop.avg_rating
                });
            // }
        }

        return MovingShopsList.sort((S1, S2) => S1.id > S2.id ? 1 : -1);
    });


    if ( ShopsList.length === 0)
        return <View style={styles.centered}>
             <CustomText isArabic={true}>No shops found!! Come back soon.</CustomText>
            <Button title='Try Again' onPress={ () => navigation.navigate('CategoriesScreen')} />
        </View>

    return (
        <FlatList
            style={styles.list}
            contentContainerStyle={styles.listContainer}
            data={ShopsList}
            keyExtractor={(Item) => {
                return Item.id.toString();
            }}
            ItemSeparatorComponent={() => {
                return (
                    <View style={styles.separator} />
                )
            }}  
              renderItem={(post) => {
                const Item = post.item;

                return (
                    <ShopItem
                        name={Item.name}
                        image={Item.cover_image}
                        logo={Item.logo_image}
                        rating={Item.avg_rating}
                        // onViewShop={() => navigation.navigate('ShopOverviewScreen', { SelectedShopID: Item.id })}
                    />
                )
            }} />     
    );
}

const styles = StyleSheet.create({
    list: {
        paddingHorizontal: 17,
        paddingVertical: 10,
        backgroundColor: "#E6E6E6",
    },
    listContainer: {
        paddingBottom: 20
    },
    separator: {
        marginVertical: 1,
    },
    centered: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});   