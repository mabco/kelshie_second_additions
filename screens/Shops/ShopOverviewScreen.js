import React,{useState,useEffect,useCallback} from 'react';
import { StyleSheet, View, Image, TouchableOpacity, Dimensions, Platform, ActivityIndicator, Button ,BackHandler, Alert } from 'react-native';
import { ScrollView, FlatList } from 'react-native-gesture-handler';
import ProductItem from '../../components/Products/ProductItem';
import { useSelector, useDispatch } from 'react-redux';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import CustomHeaderButton from '../../components/UI/HeaderButton';
import * as CartActions from '../../store/Actions/Cart';
import * as ShopsActions from '../../store/Actions/Shops';
import ReviewItem from '../../components/Shop/ReviewItem';
import { TextInput } from 'react-native-paper';
import {CustomText } from '../../components/UI/StyledText';
const { width: screenWidth } = Dimensions.get('window');
const LogoXPos = (screenWidth - 130);

export default function ShopOverviewScreen({ navigation, route }) {
    const [valid, setValid] = useState(true);
    const SelectedShopID = route.params.SelectedShopID;
    let SelectedShop = useSelector(State =>
        State.shopsReducer.allShops[SelectedShopID]
    );
    //  console.log("state update",SelectedShopID);
    const Orders = useSelector(State => State.ordersReducer.orders);

  

    navigation.setOptions({
        headerTitle:<CustomText isArabic={true}>{SelectedShop ? SelectedShop.name + ' Shop' : 'Shop'}</CustomText>,
        headerRight: () => (
            <HeaderButtons HeaderButtonComponent={CustomHeaderButton}>
                <Item title='Menu' iconName={Platform.OS === 'android' ? "md-cart" : "ios-cart"} onPress={() => navigation.navigate('CartScreen')} />
            </HeaderButtons>
        ),
        
    });
    
//Handler BackPress
//, onPress: () => validationError = false
function handleBackButtonClick() {
    if (Orders.length != 0) {
    Alert.alert(
        'Please Check Out Or Your cart will be Cleared !!!',
        '', // <- this part is optional, you can pass an empty string
        [
            { text: 'OK' },
        ],
        { cancelable: false },
    );
    navigation.navigate('CartScreen')}
    else{navigation.goBack();}
    return true;
  }

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
    };
  }, []);





    const MaxStartCount = 5;
    const FilledStarsCount = SelectedShop ? SelectedShop.avg_rating : 0;
    const EmptyStarsCount = MaxStartCount - FilledStarsCount;
    const Stars = [];

    for (let index = 0; index < FilledStarsCount; index++) {
        Stars.push(<Image key={index} style={styles.star} source={require('../../assets/images/icons/filled-star.png')} />);
    }

    for (let index = FilledStarsCount; index < EmptyStarsCount + FilledStarsCount; index++) {
        Stars.push(<Image key={index} style={styles.star} source={require('../../assets/images/icons/empty-star.png')} />);
    }

    const [IsLoading, SetIsLoading] = useState(false);
    const [IsSort, SetIsSort] = useState(false);
    const [Errors, SetErrors] = useState([]);
    const Dispatch = useDispatch();

    const Categories = useSelector(State => State.categoriesReducer.allCategories);
    const Brands = useSelector(State => State.brandsReducer.allBrands);
    const [IsSignedIn, SetIsSignedIn] = React.useState(false);
    const isLogged = useSelector(State => State.userReducer.userData.isLoggedIn);
    const LoadShopDetails = useCallback(async () => {
        try {
            await Dispatch(ShopsActions.GetShopDetails(SelectedShopID));
        } catch (error) {
            SetErrors([...Errors, error.message]);
        }
    }, [Dispatch, SetIsLoading, SetErrors]);

    const LoadShopProduct = useCallback(async () => {
        try {
            await Dispatch(ShopsActions.GetShopProducts(SelectedShopID));
        } catch (error) {
            SetErrors([...Errors, error.message]);
        }

        SetIsLoading(false);
    }, [Dispatch, SetIsLoading, SetErrors]);

    useEffect(() => {
        SetErrors([]);
        SetIsLoading(true);

        if (SelectedShop && (!SelectedShop.products || SelectedShop.products.length == 0))
            LoadShopProduct();
  
        if (!SelectedShop.reviews || !SelectedShop.address)
            LoadShopDetails();
        else
            SetIsLoading(false);
    }, [LoadShopProduct, LoadShopDetails, SetIsLoading]);

    // if (!IsLoading)
    //     SelectedShop = useSelector(State =>
    //         State.shopsReducer.allShops[SelectedShopID]
    //     );

    // const isLogged = useSelector(State => State.userReducer.userData.isLoggedIn);
    


    
     function CheckLogin(Item) {
         if (isLogged) {
            Dispatch(CartActions.AddToCart(Item))
         } else {
            navigation.navigate('DrawerNavigator', { screen: 'SignInScreen'})

         }
     }
    
     

    const [IsSearching, SetIsSearching] = useState(false);
    const [IsFilter, SetIsFilter] = useState(false);
    const CategoriesProductsLists = [];
    




    if (!IsLoading && Errors.length == 0 &&
        SelectedShop && SelectedShop.products && SelectedShop.products.length > 0) {
        let CurrentCatProducts = null;
        
       if(IsSort) {
            CurrentCatProducts = SelectedShop.products.sort((P1, P2) => P1.brand_id < P2.brand_id ? 1 : -1)
         }
        //  searchFilterFunction = text => {    
        //     CurrentCatProducts = SelectedShop.products.filter(Product => Product.name.first_name == text.first_name);
    
        //   };
        Categories.forEach(Cat => {
            if (SelectedShop.products.find(Product => Product.category_id === Cat.id)) {
                CurrentCatProducts = SelectedShop.products.filter(Product => Product.category_id == Cat.id);
                // CurrentCatProducts = CurrentCatProducts.sort((P1, P2) => P1.brand_id < P2.brand_id ? 1 : -1);
                
                CategoriesProductsLists.push(
                    <View key={Cat.id} style={styles.list}>
                        <CustomText isArabic={true} style={styles.catTitle}>{Cat.name}</CustomText>
                        
                        <FlatList
                            contentContainerStyle={styles.listContainer}
                            data={CurrentCatProducts}
                            horizontal={true}
                            initialNumToRender={10}
                            keyExtractor={(item) => {
                                return item.id.toString();
                            }}
                            ItemSeparatorComponent={() => {
                                return (
                                    <View style={styles.separator} />
                                )
                            }}
                            renderItem={(post) => {
                                const Item = post.item;

                                return (
                                    <ProductItem
                                        name={Item.name.length > 8 ? Item.name.substring(0, 8) + '...' : Item.name}
                                        showShopLogo={false}
                                        shopId={SelectedShopID}
                                        brand={Brands.find(Brand => Brand.id == Item.brand_id).name}
                                        brandIcon={Brands.find(Brand => Brand.id == Item.brand_id).icon}
                                        image={Item.images.length > 0 && Item.images[0].image_name}
                                        price={Item.shop_products[0].price}
                                        
                                        onPress={() => navigation.navigate('ProductOverviewScreen', { selectedShopID: SelectedShopID, selectedProductID: Item.id })}
                                        onViewBrandPress={() => navigation.navigate('ProductsScreen', { SelectedBrandID: Item.brand_id })}
                                        onAddToCartPress={() => CheckLogin(Item)}
                                    />
                                )
                            }}
                        />
                    </View>
                );
            }
        });
    }

    return (
        <View style={[styles.container, IsSearching ? { marginTop: -240 } : { marginTop: 0 }]}>
            <View style={styles.card} >
                <Image onError={() => setValid(false)} style={styles.cardImage} source={valid ? {uri: SelectedShop.cover_image } : require('../../assets/images/shop-banner-placeholder.jpg')} />
                <Image style={styles.logoImage} source={ valid ? { uri: SelectedShop.logo_image } : require('../../assets/images/shop-banner-placeholder.jpg') } />

                <View style={styles.cardHeader}>
                    <View style={styles.inlineItem}>
                    <CustomText isArabic={true} style={styles.title}>{SelectedShop.name}</CustomText>
                        <View style={styles.socialBarContainer}>
                            {Stars}
                        </View>
                    </View>
                </View>
            </View>

            <ScrollView>
                <View style={{ marginHorizontal: 20 }}>
                <CustomText isArabic={true}  style={styles.sectionTitle}>Available Products</CustomText>

                    {Errors.length > 0
                        && <View style={styles.centered}>
                             <CustomText isArabic={false}>Error occurred! </CustomText>
                            <Button title='Try Again' onPress={LoadShopProduct} />
                        </View>}

                    {IsLoading &&
                        <View style={styles.centered}>
                            <ActivityIndicator size='large' />
                        </View>}

                    {!IsLoading && SelectedShop && SelectedShop.products && SelectedShop.products.length === 0 &&
                        <View style={styles.centered}>
                             <CustomText isArabic={false}>No products found!! Come back soon.</CustomText>
                            <Button title='Try Again' onPress={LoadShopProduct} />
                        </View>}

                    {!IsLoading && Errors.length == 0 && SelectedShop && SelectedShop.products && SelectedShop.products.length > 0 &&
                        <View>
                            {!IsSearching &&
                                <View style={styles.contentContainer}>
                                    <TouchableOpacity style={styles.sfContainer} onPress={()=>SetIsSort(true)}>
                                        <Image style={styles.Sortimage} source={require('../../assets/images/icons/sort.png')} />
                                        <CustomText isArabic={true} style={styles.review}>Sort</CustomText>
                                    </TouchableOpacity>

                                    <TouchableOpacity style={styles.sfContainer}>
                                        <Image style={styles.filterimage, { marginLeft: 10 }} source={require('../../assets/images/icons/filter.png')}
                                        onPress={() => SetIsFilter(true)} />
                                        <CustomText isArabic={true} style={styles.review}>Filter</CustomText>
                                    </TouchableOpacity>

                                    <TouchableOpacity style={styles.sfContainer} onPress={() => SetIsSearching(prevState => !prevState)}>
                                        <Image style={styles.searchImage} source={require('../../assets/images/icons/search.png')} />
                                        <CustomText isArabic={true} style={styles.review}>Search</CustomText>
                                    </TouchableOpacity>
                                </View>
                            }

                            {IsSearching &&
                                <View style={styles.contentContainer}>
                                    <Image style={styles.searchImage} source={require('../../assets/images/icons/search.png')} />
                                    <TextInput style={styles.searchInput} autoFocus={true}  onChangeText={text => SelectedShop.products.filter((item) => { Item.name.toLowerCase().match(text) })} />
                                    <TouchableOpacity onPress={() => SetIsSearching(prevState => !prevState)}>
                                        <Image style={styles.cancelImage} source={require('../../assets/images/icons/cancel.png')} />
                                    </TouchableOpacity>
                                </View>
                            }

                            {CategoriesProductsLists}
                        </View>
                    }
                </View>

                <View style={{ marginHorizontal: 20 }}>
                <CustomText isArabic={true} style={styles.sectionTitle}>Reviews &amp; Ratings</CustomText>

                    {IsLoading &&
                        <View style={styles.centered}>
                            <ActivityIndicator size='large' />
                        </View>}

                    {!IsLoading && SelectedShop && SelectedShop.reviews && SelectedShop.reviews.length == 0 &&
                        <View style={styles.centered}>
                            <CustomText isArabic={false} >No reviews yes!!</CustomText>
                            <Button title='Try Again' onPress={LoadShopProduct} />
                        </View>}

                    {!IsLoading && SelectedShop && SelectedShop.reviews && SelectedShop.reviews.length > 0 &&
                        <View style={styles.ratingContainer}>
                            <FlatList
                                style={styles.scrollView}
                                contentContainerStyle={styles.contentScrollView}
                                data={SelectedShop.reviews}
                                initialNumToRender={10}
                                numColumns={1}
                                keyExtractor={(item) => {
                                    return item.id.toString();
                                }}
                                ItemSeparatorComponent={() => {
                                    return (
                                        <View style={styles.separator} />
                                    )
                                }}
                                renderItem={(post) => {
                                    const Item = post.item;

                                    return (
                                        <ReviewItem
                                            name={Item.user.first_name + ' ' + Item.user.last_name}
                                            time={Item.created_at}
                                            rating={Item.rating}
                                            comment={Item.comment}
                                        />
                                    )
                                }}
                            />
                        </View>
                    }
                </View>
            </ScrollView>
        </View>

    );
}

const resizeMode = 'cover';

const styles = StyleSheet.create({
    reviews: {
        fontSize: 18,
        fontWeight: 'bold',
    },
    review: {
        fontSize: 18,
        
    },
    separator: {
        height: 1,
        backgroundColor: "#CCCCCC",
    },
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    /******** card **************/
    card: {
        shadowColor: '#000',
        shadowOpacity: 1,
        shadowRadius: 0,
        elevation: 10,
        backgroundColor: "white",
        overflow: 'hidden',
        paddingBottom: 10,
    },
    cardHeader: {
        paddingVertical: 8,
        paddingHorizontal: 16,
        borderTopLeftRadius: 1,
        borderTopRightRadius: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    cardImage: {
        height: 150,
        width: screenWidth,
        borderColor: '#aaa',
        borderWidth: 1,
        resizeMode
    },
    description: {
        fontSize: 20,
        fontWeight: 'bold',
        color: "#888",
        flex: 1,
        marginBottom: 5,
    },
    /******** social bar ******************/
    socialBarContainer: {
        justifyContent: 'flex-end',
        alignItems: 'center',
        flexDirection: 'row',
        flex: 1
    },
    logoImage: {
        width: 100,
        height: 100,
        position: 'absolute',
        top: 100,
        borderWidth: 3,
        borderRadius: 10,
        borderColor: '#efefef66',
        left: LogoXPos
    },
    inlineItem: {
        alignItems: 'flex-start',
        height: 50
    },
    title: {
        fontSize: 25,
        marginTop: -4,
        width: '100%',
    },
    sectionTitle: {
        fontSize: 20,
        marginTop: 25,
        marginBottom: 15,
        borderBottomWidth: 2,
        borderBottomColor: '#ededed',
        
    },
    catTitle: {
        fontSize: 15,
        fontWeight: 'bold',
        backgroundColor: '#dcdcdc',
        paddingHorizontal: 5,
        paddingVertical: 5,
        marginHorizontal: 5,
        borderRadius: 4
    },
    star: {
        marginTop: 10,
        width: 25,
        height: 25,
    },
    contentContainer: {
        paddingVertical: 10,
        paddingHorizontal: 10,
        shadowRadius: 10,
        backgroundColor: '#fff',
        elevation: 5,
        flex: 1,
        justifyContent: 'space-between',
        flexDirection: 'row',
        borderRadius: 10,
        borderBottomLeftRadius: 0,
        borderBottomRightRadius: 0,
    },
    scrollView: {
        width: '100%',
    },
    contentScrollView: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    ratingContainer: {
        flex: 1,
        paddingVertical: 10,
        paddingHorizontal: 10,
        alignItems: 'center',
        shadowRadius: 10,
        backgroundColor: '#efefef',
        elevation: 5,
        marginBottom: 20
    },
    Sortimage: {
        width: 20,
        height: 20,
        marginTop: 3,
        marginRight: 3
    },
    filterimage: {
        width: 17,
        height: 17,
        marginTop: 3,
        marginRight: 3
    },
    searchImage: {
        width: 25,
        height: 20,
        marginTop: 3,
        marginRight: 3
    },
    cancelImage: {
        width: 25,
        height: 25,
        marginTop: 3,
        marginRight: 3
    },
    sfContainer: {
        flexDirection: 'row',
    },
    sfContainer2: {
        flexDirection: 'row-reverse',
        alignItems: 'stretch',
    },
    searchInput: {
        backgroundColor: '#fff',
        height: 25,
        width: screenWidth - 130,
        paddingHorizontal: 3,
    },
    list: {
        backgroundColor: "#efefef",
        paddingVertical: 10,
        paddingHorizontal: 10,
        shadowRadius: 10,
        elevation: 5
    },
    listContainer: {
        alignItems: 'center',
        paddingRight: 23
    },
    separator: {
        marginTop: 3,
    },
    centered: {
        flex: 1,
        marginTop: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
 
});