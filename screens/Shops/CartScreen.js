import  React , { useState, useEffect, useCallback } from 'react';
import { StyleSheet, View, Button,BackHandler } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import { useSelector, useDispatch } from 'react-redux';
import Colors from '../../constants/Colors';
import CartItem from '../../components/Shop/CartItem';
import * as CartActions from '../../store/Actions/Cart';
import * as OrdersActions from '../../store/Actions/Orders';
import  {CustomText }  from '../../components/UI/StyledText';

export default function CartScreen({ navigation }) {
    navigation.setOptions({
        headerTitle: 'My Cart',
        headerRight: () => {
            if (CartItems.length !== 0) {
                return (<View style={styles.clearCartButton}>
                    <Button onPress={() => {Dispatch(CartActions.ClearCart()); navigation.goBack(); }}
                        color='#C00'
                        title="Clear Cart"
                    />
                </View>)
            }
        },
    });

    //Handler BackPress
//, onPress: () => validationError = false
function handleBackButtonClick() {
    navigation.goBack();
    return true;
  }

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
    };
  }, []);
    const CartTotalAmount = useSelector(State => State.cartReducer.totalAmount);

    let CartItems = useSelector(State => {
        const MovingCartItems = [];

        for (const Key in State.cartReducer.items) {
           
            if (State.cartReducer.items.hasOwnProperty(Key)) {
                MovingCartItems.push({
                    productID: Key,
                    productName: State.cartReducer.items[Key].title,
                    productPrice: State.cartReducer.items[Key].price,
                    // quantity: State.cartReducer.items[Key].quantity,
                    sum: State.cartReducer.items[Key].sum,
                });
            }
        }

        return MovingCartItems.sort((P1, P2) => P1.productID > P2.productID ? 1 : -1);
    });

    const Dispatch = useDispatch();

    return (
        <View style={styles.container}>
            <View style={styles.summary}>
            <CustomText isArabic={false} style={styles.summaryText}>
                    Total:{' '}
                    <CustomText isArabic={false} style={styles.amount}>{CartTotalAmount.toFixed(2)} SP</CustomText>
                </CustomText>
                <View style={styles.orderButton}>
                    <Button
                        color={Colors.tintColor}
                        title="Checkout &amp; Order Now"
                        disabled={CartItems.length === 0}
                        onPress={() => Dispatch(OrdersActions.AddOrder(CartItems, CartTotalAmount))}
                    />
                </View>
                <CustomText isArabic={false} style={styles.notice}>Notice: Until you checkout, prices may change</CustomText>
            </View>
            <View>
                <FlatList
                    data={CartItems}
                    keyExtractor={Item => Item.productID}
                    renderItem={Item => (
                        <CartItem
                            // quantity={Item.item.quantity}
                            title={Item.item.productName}
                            price={Item.item.productPrice}
                            amount={Item.item.sum}
                            onRemove={() => { Dispatch(CartActions.RemoveFromCart(Item.item.productID)) }}
                            removable
                        />
                    )}
                />
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fafafa',
        padding: 20
    },
    summary: {
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 20,
        padding: 10,
        shadowColor: 'black',
        shadowRadius: 10,
        elevation: 7,
        backgroundColor: 'white',
        borderRadius: 10,
        // borderBottomLeftRadius: 0,
        // borderBottomRightRadius: 0
    },
    summaryText: {
        fontSize: 18
    },
    orderButton: {
        borderRadius: 7,
        overflow: 'hidden',
        width: '100%',
        marginTop: 10
    },
    notice: {
        textAlign: 'left',
        width: '100%',
        marginTop: 15,
        fontWeight: 'bold',
        color: '#AAA'
    },
    clearCartButton: {
        borderRadius: 7,
        overflow: 'hidden',
        marginRight: 15
    },
    amount: {
        fontWeight: 'bold',
        color: Colors.tintColor
    }
});