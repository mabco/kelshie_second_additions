import React, {useState,useEffect,useCallback} from 'react';
import {StyleSheet,View,FlatList,ActivityIndicator,Button} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import * as ShopsActions from '../../store/Actions/Shops';
import ShopItem from '../../components/Shop/ShopItem';
import {CustomText } from '../../components/UI/StyledText';


export default function ShopsScreen({ navigation }) {
    const [IsLoading, SetIsLoading] = useState(false);
    const [Errors, SetErrors] = useState([]);

    let ShopsList = useSelector(State => {
        const MovingShopsList = [];

        for (const Key in State.shopsReducer.allShops) {
            if (State.shopsReducer.allShops.hasOwnProperty(Key)) {
                MovingShopsList.push({
                    id: Key,
                    name: State.shopsReducer.allShops[Key].name,
                    logo_image: State.shopsReducer.allShops[Key].logo_image,
                    cover_image: State.shopsReducer.allShops[Key].cover_image,
                    closed_time: State.shopsReducer.allShops[Key].closed_time,
                    avg_rating: State.shopsReducer.allShops[Key].avg_rating
                });
            }
        }

        return MovingShopsList.sort((S1, S2) => S1.id > S2.id ? 1 : -1);
    });
    function shuffle(array) {
        var currentIndex = array.length, temporaryValue, randomIndex;
      
        // While there remain elements to shuffle...
        while (0 !== currentIndex) {
      
          // Pick a remaining element...
          randomIndex = Math.floor(Math.random() * currentIndex);
          currentIndex -= 1;
      
          // And swap it with the current element.
          temporaryValue = array[currentIndex];
          array[currentIndex] = array[randomIndex];
          array[randomIndex] = temporaryValue;
        }
      
        return array;
      }
    const Dispatch = useDispatch();

    const LoadShops = useCallback(async () => {
        SetIsLoading(true);

        try {
            await Dispatch(ShopsActions.GetAllShops());
        } catch (error) {
            SetErrors([...Errors, error.message])
        }

        SetIsLoading(false);
    }, [Dispatch, SetIsLoading, SetErrors]);

    useEffect(() => {
        SetErrors([]);
        LoadShops();
    }, [SetErrors, LoadShops]);

    // useEffect(() => {
    //     navigation.addListener('focus', LoadShops);
    // }, [LoadShops]);

    if (Errors.length > 0)
        return <View style={styles.centered}>
            <CustomText isArabic={true}>Error occurred!</CustomText>
            <Button title='Try Again' onPress={LoadShops()} />
        </View>

    if (IsLoading)
        return <View style={styles.centered}>
            <ActivityIndicator size='large' />
        </View>

    if (!IsLoading && ShopsList.length === 0)
        return <View style={styles.centered}>
             <CustomText isArabic={true}>No shops found!! Come back soon.</CustomText>
            <Button title='Try Again' onPress={LoadShops} />
        </View>

    return (
        <FlatList
            style={styles.list}
            contentContainerStyle={styles.listContainer}
            data={shuffle(ShopsList)}
            keyExtractor={(Item) => {
                return Item.id.toString();
            }}
            ItemSeparatorComponent={() => {
                return (
                    <View style={styles.separator} />
                )
            }}  
              renderItem={(post) => {
                const Item = post.item;

                return (
                    <ShopItem
                        name={Item.name}
                        image={Item.cover_image}
                        logo={Item.logo_image}
                        rating={Item.avg_rating}
                        onViewShop={() => navigation.navigate('ShopOverviewScreen', { SelectedShopID: Item.id })}
                    />
                )
            }} />     
    );
}

const styles = StyleSheet.create({
    list: {
        paddingHorizontal: 17,
        paddingVertical: 10,
        backgroundColor: "#E6E6E6",
    },
    listContainer: {
        paddingBottom: 20
    },
    separator: {
        marginVertical: 1,
    },
    centered: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});   